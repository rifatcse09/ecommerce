<?php
	require_once('functions/cls_dbconfig.php');
	function __autoload($classname){
	  require_once("functions/$classname.class.php");
	}
	
	$cls_root_category = new cls_root_category();
	$cls_additem = new cls_additem();
	$cls_slider = new cls_slider();
	$cls_all_product = new cls_all_product();
	$cls_registration = new cls_registration();
	$cls_customer_login = new cls_customer_login();
	$cls_customer = new cls_customer();
	
	$root_data = $cls_root_category->view_data_root();
	
?>
<?php
		session_start();
		$uniq_id = $_SESSION['uniq_id'];	
		$customer_id = $_SESSION['customer_id'];
		
		$maisha_q = $cls_additem->add_item($uniq_id);
		
		$rana_r = $maisha_q->fetch_assoc();
		$book = $rana_r['book_id'];
		
		$maisha_q1 =  $cls_additem->select_temp_details_sum($uniq_id);
		$rana_r1 = $maisha_q1->fetch_array();
		
		$t_qty = $rana_r1[0];		
		list($total_cart_item, $zero) = split('[/.]', $t_qty);
		$total_cart_item;
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
       <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Dhaka CentreNIC IT Limited"/>
         <title><?php require_once("title.php"); ?> </title>
        <link rel="icon" type="img/ico" href="images/favicon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="responsive.css" type="text/css">
		   <link rel="stylesheet" href="css/style.css" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:600,700,800,400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="js/modernizr-2.8.3.min.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/slick.min.js"></script>
        <script type="text/javascript" src="js/superbox.min.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/scrollTop.js"></script>
        <script type="text/javascript" src="js/nahian.js"></script>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	
	<link rel="stylesheet" href="alert/alertify.min.css">
   <link rel="stylesheet" href="alert/default.min.css">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/styles-imgzoom.css" rel="stylesheet" type="text/css" media="all" />
  </head>
  <body>
       <header>
            <section class="header-top full">
                <div class="container">
                   <div class="col-md-7">
				   <h2>Al Jadeed Fashion</h2>
                </div>
                
                <div class="col-md-5">
               <div class="header-right">
					<div id="navbar" style="height:15px;">    
					<nav class="navbar navbar-default navbar-static-top" role="navigation">
					<div class="collapse navbar-collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user">My Account</i> <b class="caret"></b></a> 
                      
                        <ul class="dropdown-menu">
                          <li id="dropdown-login"><a href="myaccount.php">My Account</a></li>
						  <li id="dropdown-login"><a href="#">My Order  </a></li>
                            <li id="dropdown-login"><a href="#" ><span id="signout">Log Out</span></a></li>
                                                           
                        </ul>
                    </li>
					<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart">Add Crat</i></a> 
                        <ul class="dropdown-menu">
						<div class="osh-popover-arrow"></div>
						<div class="count" style="color:black;text-align:center;font-size:12px;"><b>There are <?php if(!empty($total_cart_item)) { ?><?php echo $total_cart_item; ?><?php } else { ?>0<?php } ?> items</a> in your cart.</b></div>
						<div class="items">
						 <?php
						 
							$sub_total = 0; $total = 0;
							$maishade_q = $cls_additem->select_book_id_book_qty($uniq_id);
							while($ra2 = $maishade_q->fetch_array())
							{
								
								$book_id = $ra2[0];
								$book_qty = $ra2[1];
								$book_price = $ra2[2];
								
								$book_price_decimal = number_format($book_price, 2, '.', ',');
								
								$maisha2 = $cls_additem->select_title_book_image($book_id);
								$ras2 = $maisha2->fetch_array();
								
								$booktitle = $ras2[0];
								$bookimage = $ras2[1];
								$editor = $ras2[2];
								
								$edm =  $cls_additem->select_cat_name($editor);
								$edr = $edm->fetch_array();
								$editor_name = $edr[0];
																
								$total_price = $book_qty*$book_price;
								
								$sub_total = $sub_total + $total_price;
								$sub_total_decimal = number_format($sub_total, 2, '.', ',');
																
								//$total = $sub_total + $delivery_charge;

							?>
						<div class="item">
								<div class="ul-image">
								<a href="#"><img src="../apanel/title_image/<?php echo $bookimage; ?>" width="50" height="50"/> </a>
								</div>
								<div class="ul-info">
								<li style="text-transform: uppercase;"><a href="product_details.php?ProductID=<?php echo $pro_id; ?>"><?php echo $booktitle; ?></a></li>
								<!--<p class="size-ul">Size:M <span class="remove"> <a href="#"/>Remove</a></span></p>-->
								<li style="color:black;float:left;font-size:10px;">Quantity:<?php echo $book_qty; ?> X Unit price: <?php echo $book_price_decimal; ?> (TK)</li>
								</div>
								<p class="size-ul">Size:M <span class="remove"> <a href="#"/>Remove</a></span></p>
						</div>
							<?php } ?>
						</div>
						<div class="total-ul">
						   <span class="total-span"> Subtotal:</span> <?php echo $sub_total_decimal; ?> (TK) 
						</div>
						<div class="view-cart">
						   <div class="view-right">
						     <a href="shoppingcart.php" class="btn btn-warning btn-lg">Go to cart</a>
						   </div>
						   <div class="view-left">
							 <a href="shipping.php?confirm_id=<?php echo $customer_id; ?>" class="btn btn-warning btn-lg">Checkout</a>
						   </div>
						</div>
                        </ul>
                    </li>
					<li class="last"><a style="color:white;">(<?php if(!empty($total_cart_item)) { ?><?php echo $total_cart_item; ?><?php } else { ?>0<?php } ?>)</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
			</nav>
			</div>
			</div>
           </div><!-- end col-md-5-->
       </div><!--container end-->
    </section><!--header-top end-->
            <section class="header" style="background-color:">
                <div class="container">
				<div class="row-row">
                    <div class="col-md-6 col-sm-6 header-menu pd0">
					<div class="logo">
                        <a href="#"><img src="img/log3.png" style="height:110px;width:60%;"></a>
                    </div>
                    </div>
                    <div class="col-md-6 col-sm-6 pd0">
                        <div class="search"><br>
                            <input type="text" placeholder="Search">
                            <button><br>
                            	<span class="fa fa-search"></span>
                            </button>
                        </div>
                    </div><!--col-md-4 end-->
					</div>
                </div><!--container end-->
            </section><!--header ends-->
        </header><!--header part ends here-->

<div id="nav-n-slider">
    <nav class="navbar navBg" role="navigation">
    <div class="container pd0">
        <div class="col-md-12 col-sm-10 pd0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nahianNav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!--navbar-header end-->
            <div class="collapse navbar-collapse" id="nahianNav">
                <ul class="navbar-nav">
                    <li><a href="home.php" class="active">home</a>
                    </li>
					
					
					<?php while($cat = $root_data->fetch_assoc()){?>
					
                       <li class="dropdown">
                        <a href="#" ><?php echo $cat['root_cat_name']; ?>
						<input type="hidden" name="id" value="<?php echo $root_cat_id = $cat['id']; ?>"/>				
						</a>
                        <ul class="megaMenu dropdown-menu" style="display:<?php// if($root_cat_id != ""){ echo "0"; } ?>;">
                            <div class="nmMenu" >
							<?php 
								 $cat_data = $cls_root_category->view_data_cat($root_cat_id);
								
								while($root_cat = $cat_data->fetch_assoc()){ 
									
									$cate_id = $root_cat['id'];
									
									
									
								?>
								
                                <li class="nm01" >
                                    <a href="categories.php?root_cat=<?php echo $root_cat_id; ?>&cat=<?php echo $cate_id; ?>">
									 <div class="text">
										<h4> <?php echo $root_cat['cat_name']; ?> </h4>
									
                                        <!-- <div class="icon text-center">
                                            <img src="images/mmenu/gd.png" alt="gd.png">
                                        </div>
                                        <div class="text">
                                            <h4>T-Shirt</h4>
                                            <p>Digital Drawing Color</p>
                                        </div>-->
										</div>
                                    </a>
                                </li>
                                <!--/.nm01-->
								<?php } ?>
                            </div>

                        </ul>
                     
                    </li>
					<?php } ?>
					
					
                       <!--<li class="dropdown">
                        <a href="#" >Women's Wear</a>
                        <ul class="megaMenu dropdown-menu">
                            <div class="nmMenu">
                                <li class="nm01">
                                    <a href="#">
                                        <div class="icon text-center">
                                            <img src="images/mmenu/gd.png" alt="gd.png">
                                        </div>
                                        <div class="text">
                                            <h4>saree</h4>
                                            <p>Digital Drawing </p>
                                        </div>
                                    </a>
                                </li>
                                <!--/.nm01-->
                                
                               
                                <!--/.nm01-->

                              <!--  <li class="nm01">
                                    <a href="#">
                                        <div class="icon text-center">
                                            <img src="images/mmenu/gd.png" alt="gd.png">
                                        </div>
                                        <div class="text">
                                            <h4>T-Shirt</h4>
                                            <p>Digital Drawing Color</p>
                                        </div>
                                    </a>
                                </li>
                                <!--/.nm01-->
                             <!--  <li class="nm01">
                                    <a href="#">
                                        <div class="icon text-center">
                                            <img src="images/mmenu/gd.png" alt="gd.png">
                                        </div>
                                        <div class="text">
                                            <h4>T-Shirt</h4>
                                            <p>Digital Drawing Color</p>
                                        </div>
                                    </a>
                                </li>
                                <!--/.nm01-->

                                
                                <!--/.nm01-->

                              
                                <!--/.nm01-->
                                
                               <!-- <li class="nm01">
                                    <a href="webdevelopment_details.php#link">
                                        <div class="icon text-center">
                                            <img src="images/mmenu/wdev.png" alt="wdev.png">
                                        </div>
                                        <div class="text">
                                            <h4> Leggings & Pants</h4>
                                            <p> Leggings & Pants</p>
                                        </div>
                                    </a>
                                </li>
                                <!--/.nm01-->

                                <!-- <li class="nm01">
                                    <a href="#">
                                        <div class="icon text-center">
                                            <img src="images/mmenu/gd.png" alt="gd.png">
                                        </div>
                                        <div class="text">
                                            <h4>T-Shirt</h4>
                                            <p>Digital Drawing Color</p>
                                        </div>
                                    </a>
                                </li>
                                <!--/.nm01-->
                                
                               <!--  <li class="nm01">
                                    <a href="#">
                                        <div class="icon text-center">
                                            <img src="images/mmenu/gd.png" alt="gd.png">
                                        </div>
                                        <div class="text">
                                            <h4>T-Shirt</h4>
                                            <p>Digital Drawing Color</p>
                                        </div>
                                    </a>
                                </li>
                                <!--/.nm01-->

                              <!--   <li class="nm01">
                                    <a href="#">
                                        <div class="icon text-center">
                                            <img src="images/mmenu/gd.png" alt="gd.png">
                                        </div>
                                        <div class="text">
                                            <h4>T-Shirt</h4>
                                            <p>Digital Drawing Color</p>
                                        </div>
                                    </a>
                                </li>
                                <!--/.nm01-->
                                
                                <!-- <li class="nm01">
                                    <a href="#">
                                        <div class="icon text-center">
                                            <img src="images/mmenu/gd.png" alt="gd.png">
                                        </div>
                                        <div class="text">
                                            <h4>T-Shirt</h4>
                                            <p>Digital Drawing Color</p>
                                        </div>
                                    </a>
                                </li>
								   <li class="nm01">
                                    <a href="#">
                                        <div class="icon text-center">
                                            <img src="images/mmenu/gd.png" alt="gd.png">
                                        </div>
                                        <div class="text">
                                            <h4>T-Shirt</h4>
                                            <p>Digital Drawing Color</p>
                                        </div>
                                    </a>
                                </li>

                            </div>

                        </ul>
                     
                    </li>
                    <li class="dropdown"><a href="#" data-toggle="dropdown" >GALLERIES</a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="stu_gallery.php#link">Student Gallery </a>
                            </li>
                            <!--<li><a href="teach_gallery.php#link">Teachers Gallery</a>
                            </li>-->
                           <!-- <li><a href="office_gallery.php#link">Office Gallery</a>
                            </li>
                            <li><a href="dcit_events.php#link">DCIT events</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" data-toggle="dropdown" >STUDENT'S ACTIVITY</a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">student works </a>
                            </li>
                           <!-- <li><a href="#">achivements & awards </a>
                            </li>-->
                          <!--  <li><a href="stu_placements.php#link">students placements</a>
                            </li>
                           <!-- <li><a href="scholarship.php#link">scholarship</a>-->
                         <!--   </li>
                            <li><a href="#">Students Feedback</a>
                            </li>
                        </ul>
                    </li>
                    <!--<li><a href="#" >ONLINE TUTORIALS</a>
                    </li>-->
				<!--	<li><a href="apply_online.php#link">Registration Now</a>
                    </li>
					<li><a href="logodesign.php#link" >Logo Design</a>
                    </li>
                    <li><a href="contact_us.php#link" >CONTACT US</a>
                    </li>-->
                </ul>
            </div>
        </div>
       </div>
   
    <!--container end-->
 </nav>
 </div>
 