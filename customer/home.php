<?php 
	include('include/header.php');
	$prottyp = $cls_all_product->view_producttype();
	 $view_all_slider=$cls_slider->view_slider();
	
	if(!isset($_SESSION['customer_id'])){
		//echo "<script>alert('Session not found');</script>";
		echo "<script>location.href='../index.php';</script>";
	}
?>
  <div class="slider-area">
 <div class="container">
		<div class="col-md-9">
			<div class="slider-area" style="margin-left:25px;">
        	<!-- Slider -->
			<div class="block-slider block-slider4">
				<ul class="" id="bxslider-home4">
				<?php 
					while($result=$view_all_slider->fetch_assoc()){
						?>
						
					<li>
					
						<img src="../apanel/slider/<?php echo $result['image_name'];?>" alt="Slide">
					</li>
					<?php
					}
					?>
				</ul>
			</div>
			<!-- ./Slider -->
    </div> <!-- End slider area -->
	</div><!-- End col-md-8-->
	<div class="col-md-3" id="col-right">
		<div class="right-div">
			<div class="top-right">
				<div class="top1">
				<a href="#"><h1>50%</h1>
						<h3>SALL OFF</h3></a>
				</div>
				<div class="top2">
				<a href="#"><img src="img/headset.png" alt=""/>
				</div>
			</div>
			<div class="down-right">
				<div class="top3">
					<a href="#"><h2>30%</h2>
						<h3>SALL OFF</h3>
				</a>
				</div>
				<div class="top4">
					<a href="#"><img src="img/headset.png" alt=""/></a>
				</div>
			</div>
		</div>
	</div>
</div><!-- end container-->
</div>
    <div class="container">
		<div class="row-row">
			 <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo1">
                        <i class="fa fa-refresh"></i>
                        <p>30 Days return</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo2">
                        <i class="fa fa-truck"></i>
                        <p>Free shipping</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo3">
                        <i class="fa fa-lock"></i>
                        <p>Secure payments</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo4">
                        <i class="fa fa-gift"></i>
                        <p>New products</p>
                    </div>
                </div>
		</div>
	</div>
	<?php
		while($pr_typ = $prottyp->fetch_assoc())
		{
		$typId = $pr_typ['id'];
	?>
    
    <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row-row">
                <div class="col-md-12">
                    <div class="latest-product">
                        <a href="#"><h2 class="section-title" style="color:black"><?php echo $pr_typ['typename']; ?></h2></a>
                        <div class="product-carousel">
						
						<?php 
							$data_row = $cls_all_product->view_all_product($typId);
							
							while($view_pro = $data_row->fetch_assoc())
							
						{
							$bid = $view_pro['id'];
							
							?>
						
						 <div class="single-product">
                                <div class="product-f-image" style="height:260px" >
                                    <img src="../apanel/title_image/<?php echo $view_pro['image']; ?>" height="100%" alt="">
                                    <div class="product-hover">
                                        <a href="single-product.php?book_id=<?php echo $bid; ?>" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <a href="single-product.php?book_id=<?php echo $bid; ?>" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                    </div>
                                </div>
                                
                                <h2><a href="single-product.php?book_id=<?php echo $bid; ?>"><?php echo $view_pro['title'];?></a></h2>

                                <div class="product-carousel-price">
                                    <ins><?php echo $view_pro['price'];?></ins>
                                </div>                            
                            </div>
                            <?php
						}
							?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End main content area -->
		<?php
		} 
		?>
	
<?php
	include('include/footer.php');

 ?>