<?php 
	include('include/header.php');
		
		$root_cat = $_GET['root_cat'];
		$cat = $_GET['cat'];
	
	    $pro_data = $cls_root_category->product_view_by_id($root_cat,$cat);
		
		if(!isset($_SESSION['customer_id'])){
		//echo "<script>alert('Session not found');</script>";
		echo "<script>location.href='../index.php';</script>";
	}
		
?>

	  <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
		 <div class="row-row">
			<div class="col-md-12">
				<div class="latest-product">
				<?php while($row = $pro_data->fetch_assoc()){
					  
					 $pro_id = $row['id'];
					 
					 
					  
					
				?>
		
				<div class="col-md-3">
					 <div class="single-product">
						<div class="product-f-image" style="height:260px">
						<?php 
						$img = $cls_root_category->book_page_view_image($pro_id);
						
						$row_img = $img->fetch_assoc();
							
							
							?>
								
							<img src="../apanel/uploaded/<?php echo $row_img['photo']; ?>" style="height:100%">
							
						<?php //} ?>
							
							<div class="product-hover">
								<a href="single-product.php?book_id=<?php echo $pro_id; ?>" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
								<a href="single-product.php?book_id=<?php echo $pro_id;?>" class="view-details-link"><i class="fa fa-link"></i> See details</a>
							</div>
						</div>
					
						<h2 style="padding-top:8px;"><a href="single-product.php?book_id=<?php echo $pro_id; ?>" ><?php echo $row['title']; ?></a></h2>

						<div class="product-carousel-price">
							<ins><?php echo $row['price']; ?>.Tk</ins>
						</div>                            
					</div>
				</div>
			<?php } ?>
			
		</div>	
		</div>	
		</div>	
		
            
        </div>
    </div> <!-- End main content area -->
<?php 
	include('include/footer.php'); 
?>