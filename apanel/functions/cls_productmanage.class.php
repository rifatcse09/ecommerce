<?php
	class cls_productmanage{
		public function con(){
			$connect = new cls_dbconfig();
			return $connect->connection();
		}
		
		public function view_rootcategory(){
			$result = $this->con()->query("SELECT * from root_category");
			return $result;
		}
		
		public function get_category($div_id){
			$result = $this->con()->query("SELECT * FROM category where root_cat_id = '$div_id'");
			return $result;
		}
		public function get_subcategory($div_sub){
			$result = $this->con()->query("SELECT * FROM sub_category where cat_id = '$div_sub'");
			return $result;
		}
		public function view_producttype(){
			$result = $this->con()->query("SELECT * from producttype");
			return $result;
		}
		
		public function insert_product($root_name,$category_name,$subcate_name,$product_type,$title,$pic,$quntity,$price,$discount,$editor1,$termscon){
			
			$r = rand(1111,9999);
			$n = "$r.jpg";
		   
			if($pic != ""){
			$source_file = $pic;
			$destination = "../title_image/".$n;
			move_uploaded_file($source_file,$destination);
		   }
			
			$result = $this->con()->query("INSERT INTO book(rootcat_id,cat_id,subcat_id,pro_type,title,quantity,price,discount_price,image,book_summary,terms_condition)VALUES('$root_name','$category_name','$subcate_name','$product_type','$title','$quntity','$price','$discount','$n','$editor1','$termscon')");
			return $result;
		}
		public function image_insert($pic){
			
            //Make sure we have a filepath
            if($pic != ""){
				
				$r = rand('1111','9999');
				$n = "$r.jpg";
				
				$filePath = "../uploaded/$n";
				
                if(move_uploaded_file($pic, $filePath)) {
					
					$res = $this->con()->query("SELECT MAX(id) as maxid FROM book");
					
					$row= $res->fetch_assoc();
					$c = $row['maxid'];
					
					$res = $this->con()->query("SELECT id FROM book where id='$c'");
					$rows = $res->fetch_assoc();
					$pcode = $rows['id'];
					
					
					$result = $this->con()->query("INSERT INTO image_product(product_id,photo)VALUES('$pcode','$n')");
					return $result;

					
              
			}
			
		 }
		}
		
		public function product_view(){
			$result = $this->con()->query("SELECT book.*, root_category.root_cat_name,category.cat_name,sub_category.subcat_name FROM book JOIN root_category ON book.rootcat_id=root_category.id JOIN category ON book.cat_id=category.id JOIN sub_category ON book.subcat_id=sub_category.id ");
			return $result;
		}
		public function book_page_view_image($product_id){
			$result = $this->con()->query("SELECT * FROM image_product where product_id='$product_id' LIMIT 0,3 ");
			return $result;
		}
		public function product_view_by_id($update_id){
			$result = $this->con()->query("SELECT book.*, root_category.root_cat_name,category.cat_name,sub_category.subcat_name,image_product.photo FROM book JOIN root_category ON book.rootcat_id=root_category.id JOIN category ON book.cat_id=category.id JOIN sub_category ON book.subcat_id=sub_category.id JOIN image_product ON book.id=image_product.product_id WHERE book.id='$update_id'");
			return $result;
		}
		
	}

?>