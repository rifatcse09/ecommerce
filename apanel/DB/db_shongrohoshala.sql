-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2016 at 12:05 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_shongrohoshala`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL,
  `rootcat_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `pro_type` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `editor` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float(11,2) NOT NULL,
  `discount` varchar(200) NOT NULL,
  `discount_price` float(11,2) NOT NULL,
  `image` varchar(200) NOT NULL,
  `subject` text CHARACTER SET utf8 NOT NULL,
  `availability` text NOT NULL,
  `noofpage` text NOT NULL,
  `publisher` int(11) NOT NULL,
  `edition` text CHARACTER SET utf8 NOT NULL,
  `country` varchar(200) NOT NULL,
  `language` text NOT NULL,
  `book_summary` longtext CHARACTER SET utf8 NOT NULL,
  `terms_condition` varchar(250) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_time` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `rootcat_id`, `cat_id`, `subcat_id`, `pro_type`, `title`, `editor`, `quantity`, `price`, `discount`, `discount_price`, `image`, `subject`, `availability`, `noofpage`, `publisher`, `edition`, `country`, `language`, `book_summary`, `terms_condition`, `date`, `date_time`) VALUES
(1, 1, 3, 0, 1, 'নাইন্টিন সেভেন্টিওয়ান', 31, 0, 1240.00, '25', 930.00, '1425892527.gif', '', 'Available at publisher&#039;s end', '500', 46, '২য়', '', 'Bangla', '<p><strong>বইয়ে যা আছে</strong><br /><br /><span>মুখবন্ধ: হাবিবুল বাশার/দেবব্রতর কৈফিয়ত/</span><br /><br /><span>সাকিবের সাকিব/</span><br /><span>আকাশ ছোঁয়ার স্বপ্ন দেখি: ২০০৯/</span><br /><span>এখনও অনেক পথ পাড়ি দিতে হবে: ২০১১/</span><br /><span>নিজেকে হারাতে চাই না: ২০১৪/</span><br /><span>তাহাদের সাকিব/</span><br /><span>আমার ফয়সাল আগের মতোই আছে: শিরিন আখতার; সাকিবের মা/</span><br /><span>নিজের স্বপ্ন সত্যি করেছে ফয়সাল: মাশরুর রেজা; সাকিবের বাবা/</span><br /><span>আমার ভাইয়া লুডুতেও সেরা: জান্নাতুল ফেরদৌস রিতু/</span><br /><span>ওকে নিয়ে আমার ভয় নেই: উম্মে আহমেদ শিশির/</span><br /><span>আমার আর কিচ্ছু চাওয়ার নেই: সাদ্দাম হোসেন গের্কি/</span><br /><span>আমরা ছিলাম চ্যাম্পিয়ন: সৈয়দ তারিক আনাম প্রতীক: সাকিবের বাল্যবন্ধু/</span><br /><span>একদিন যেন দেশকে সেরা করতে পারে: সালাউদ্দিন আহমেদ; কোচ/</span><br /><span>চ্যাম্পিয়নরা উদ্ধতই হয়: জেমি সিডন্স/</span><br /><span>সর্বকালের সেরাদের একজন হতে হবে: মাশরাফি বিন মুর্তজা/</span><br /><span>সত্যিকারের চ্যাম্পিয়ন ক্রিকেটার: তামিম ইকবাল/</span><br /><span>সাকিবের সঙ্গে এক দলে খেলাটা ভাগ্যের ব্যাপার: মুশফিকুর রহিম/</span><br /><span>সাকিবের মূল্যায়ন যথার্থ হয় না: আব্দুর রাজ্জাক/</span><br /><span>সে অলরাউন্ডার ক্লাবের যোগ্য সদস্য: জ্যাক ক্যালিস/</span><br /><span>সাকিব ভাই আমাদের প্রেরণা: সালমা খাতুন/</span><br /><span>আমিও সাবিবের ভক্ত: জাহিদ হোসেন এমিলি/</span><br /><span>চাকচিক্যে ভোলেনি সাকিব: নিয়াজ মোরশেদ/</span><br /><span>চাপটা সবচেয়ে ভালো সামলাতে পারে: আতহার আলী খান/</span><br /><span>বিস্ময়কর কমিটমেন্ট: সঞ্জয় মাঞ্জরেকার/</span><br /><span>সাকিবের যত্ন নিতে হবে: ওয়াসিম আকরাম/</span><br /><span>বাংলাদেশকে নতুন উচ্চতায় নেবে: সাকলাইন মুশতাক/</span><br /><span>মিডিয়া গেমটা খুব ভালো বোঝে: অ্যান্ডু মিলার/</span><br /><span>ভিনু মানকড়ের সঙ্গে তুলনা করতে চাই: শিল্ড বেরি/</span><br /><span>আমাদের আন্তর্জতিক পরিচয় সাকিব: আনিসুল হক/</span><br /><span>রোজকার দেখা চরিত্র নয় সাকির: উৎপল শুভ্র/</span><br /><span>সাকিবের এবং ক্রিকেটের মঙ্গল চাই: মোস্তফা মামুন/</span><br /><span>বাংলাদেশের সবচেয়ে ভুল বোঝো মানুষ: রাবিদ ইমাম/</span><br /><span>আমাদের সাকিব/</span><br /><span>মাগুরা থোক এভারেস্ট/ (সাকিবের সংক্ষিপ্ত বায়োগ্রাফি)</span><br /><span>এক নজরে সাকিব/</span></p>', '', 'Dec 13, 2014', 'Dec 13, 2014'),
(2, 1, 3, 0, 2, 'যখন আমি মোটকু ছিলাম', 33, 0, 300.00, '12', 264.00, '1425891262.gif', '', 'Available at publisher&#039;s end', '', 60, '', '', 'Bangla', '<p><strong>বইয়ে যা আছে</strong><br /><br /><span>মুখবন্ধ: হাবিবুল বাশার/দেবব্রতর কৈফিয়ত/</span><br /><br /><span>সাকিবের সাকিব/</span><br /><span>আকাশ ছোঁয়ার স্বপ্ন দেখি: ২০০৯/</span><br /><span>এখনও অনেক পথ পাড়ি দিতে হবে: ২০১১/</span><br /><span>নিজেকে হারাতে চাই না: ২০১৪/</span><br /><span>তাহাদের সাকিব/</span><br /><span>আমার ফয়সাল আগের মতোই আছে: শিরিন আখতার; সাকিবের মা/</span><br /><span>নিজের স্বপ্ন সত্যি করেছে ফয়সাল: মাশরুর রেজা; সাকিবের বাবা/</span><br /><span>আমার ভাইয়া লুডুতেও সেরা: জান্নাতুল ফেরদৌস রিতু/</span><br /><span>ওকে নিয়ে আমার ভয় নেই: উম্মে আহমেদ শিশির/</span><br /><span>আমার আর কিচ্ছু চাওয়ার নেই: সাদ্দাম হোসেন গের্কি/</span><br /><span>আমরা ছিলাম চ্যাম্পিয়ন: সৈয়দ তারিক আনাম প্রতীক: সাকিবের বাল্যবন্ধু/</span><br /><span>একদিন যেন দেশকে সেরা করতে পারে: সালাউদ্দিন আহমেদ; কোচ/</span><br /><span>চ্যাম্পিয়নরা উদ্ধতই হয়: জেমি সিডন্স/</span><br /><span>সর্বকালের সেরাদের একজন হতে হবে: মাশরাফি বিন মুর্তজা/</span><br /><span>সত্যিকারের চ্যাম্পিয়ন ক্রিকেটার: তামিম ইকবাল/</span><br /><span>সাকিবের সঙ্গে এক দলে খেলাটা ভাগ্যের ব্যাপার: মুশফিকুর রহিম/</span><br /><span>সাকিবের মূল্যায়ন যথার্থ হয় না: আব্দুর রাজ্জাক/</span><br /><span>সে অলরাউন্ডার ক্লাবের যোগ্য সদস্য: জ্যাক ক্যালিস/</span><br /><span>সাকিব ভাই আমাদের প্রেরণা: সালমা খাতুন/</span><br /><span>আমিও সাবিবের ভক্ত: জাহিদ হোসেন এমিলি/</span><br /><span>চাকচিক্যে ভোলেনি সাকিব: নিয়াজ মোরশেদ/</span><br /><span>চাপটা সবচেয়ে ভালো সামলাতে পারে: আতহার আলী খান/</span><br /><span>বিস্ময়কর কমিটমেন্ট: সঞ্জয় মাঞ্জরেকার/</span><br /><span>সাকিবের যত্ন নিতে হবে: ওয়াসিম আকরাম/</span><br /><span>বাংলাদেশকে নতুন উচ্চতায় নেবে: সাকলাইন মুশতাক/</span><br /><span>মিডিয়া গেমটা খুব ভালো বোঝে: অ্যান্ডু মিলার/</span><br /><span>ভিনু মানকড়ের সঙ্গে তুলনা করতে চাই: শিল্ড বেরি/</span><br /><span>আমাদের আন্তর্জতিক পরিচয় সাকিব: আনিসুল হক/</span><br /><span>রোজকার দেখা চরিত্র নয় সাকির: উৎপল শুভ্র/</span><br /><span>সাকিবের এবং ক্রিকেটের মঙ্গল চাই: মোস্তফা মামুন/</span><br /><span>বাংলাদেশের সবচেয়ে ভুল বোঝো মানুষ: রাবিদ ইমাম/</span><br /><span>আমাদের সাকিব/</span><br /><span>মাগুরা থোক এভারেস্ট/ (সাকিবের সংক্ষিপ্ত বায়োগ্রাফি)</span><br /><span>এক নজরে সাকিব/</span></p>', '', 'Dec 13, 2014', 'Dec 13, 2014'),
(3, 1, 3, 0, 3, 'বাংলাদেশ : নাইন্টিন সেভেন্টিওয়ান', 32, 0, 1240.00, '25', 930.00, '1425892561.gif', '', 'Available at publisher&#039;s end', '500', 46, '২য়', '', 'Bangla', '<p><strong>বইয়ে যা আছে</strong><br /><br /><span>মুখবন্ধ: হাবিবুল বাশার/দেবব্রতর কৈফিয়ত/</span><br /><br /><span>সাকিবের সাকিব/</span><br /><span>আকাশ ছোঁয়ার স্বপ্ন দেখি: ২০০৯/</span><br /><span>এখনও অনেক পথ পাড়ি দিতে হবে: ২০১১/</span><br /><span>নিজেকে হারাতে চাই না: ২০১৪/</span><br /><span>তাহাদের সাকিব/</span><br /><span>আমার ফয়সাল আগের মতোই আছে: শিরিন আখতার; সাকিবের মা/</span><br /><span>নিজের স্বপ্ন সত্যি করেছে ফয়সাল: মাশরুর রেজা; সাকিবের বাবা/</span><br /><span>আমার ভাইয়া লুডুতেও সেরা: জান্নাতুল ফেরদৌস রিতু/</span><br /><span>ওকে নিয়ে আমার ভয় নেই: উম্মে আহমেদ শিশির/</span><br /><span>আমার আর কিচ্ছু চাওয়ার নেই: সাদ্দাম হোসেন গের্কি/</span><br /><span>আমরা ছিলাম চ্যাম্পিয়ন: সৈয়দ তারিক আনাম প্রতীক: সাকিবের বাল্যবন্ধু/</span><br /><span>একদিন যেন দেশকে সেরা করতে পারে: সালাউদ্দিন আহমেদ; কোচ/</span><br /><span>চ্যাম্পিয়নরা উদ্ধতই হয়: জেমি সিডন্স/</span><br /><span>সর্বকালের সেরাদের একজন হতে হবে: মাশরাফি বিন মুর্তজা/</span><br /><span>সত্যিকারের চ্যাম্পিয়ন ক্রিকেটার: তামিম ইকবাল/</span><br /><span>সাকিবের সঙ্গে এক দলে খেলাটা ভাগ্যের ব্যাপার: মুশফিকুর রহিম/</span><br /><span>সাকিবের মূল্যায়ন যথার্থ হয় না: আব্দুর রাজ্জাক/</span><br /><span>সে অলরাউন্ডার ক্লাবের যোগ্য সদস্য: জ্যাক ক্যালিস/</span><br /><span>সাকিব ভাই আমাদের প্রেরণা: সালমা খাতুন/</span><br /><span>আমিও সাবিবের ভক্ত: জাহিদ হোসেন এমিলি/</span><br /><span>চাকচিক্যে ভোলেনি সাকিব: নিয়াজ মোরশেদ/</span><br /><span>চাপটা সবচেয়ে ভালো সামলাতে পারে: আতহার আলী খান/</span><br /><span>বিস্ময়কর কমিটমেন্ট: সঞ্জয় মাঞ্জরেকার/</span><br /><span>সাকিবের যত্ন নিতে হবে: ওয়াসিম আকরাম/</span><br /><span>বাংলাদেশকে নতুন উচ্চতায় নেবে: সাকলাইন মুশতাক/</span><br /><span>মিডিয়া গেমটা খুব ভালো বোঝে: অ্যান্ডু মিলার/</span><br /><span>ভিনু মানকড়ের সঙ্গে তুলনা করতে চাই: শিল্ড বেরি/</span><br /><span>আমাদের আন্তর্জতিক পরিচয় সাকিব: আনিসুল হক/</span><br /><span>রোজকার দেখা চরিত্র নয় সাকির: উৎপল শুভ্র/</span><br /><span>সাকিবের এবং ক্রিকেটের মঙ্গল চাই: মোস্তফা মামুন/</span><br /><span>বাংলাদেশের সবচেয়ে ভুল বোঝো মানুষ: রাবিদ ইমাম/</span><br /><span>আমাদের সাকিব/</span><br /><span>মাগুরা থোক এভারেস্ট/ (সাকিবের সংক্ষিপ্ত বায়োগ্রাফি)</span><br /><span>এক নজরে সাকিব/</span></p>', '', 'Dec 13, 2014', 'Dec 13, 2014'),
(4, 1, 47, 0, 4, 'অন্নপূর্ণায়', 77, 0, 280.00, '14', 240.80, '1420468790.gif', '', 'Available at publisher''s end', '200', 47, '2nd', 'Bangladesh', 'Bangla', '<p><strong>বই সম্পর্কেঃ</strong><br /><span>প্রায় ত্রিশটির বেশি দেশ ঘুরে আসার পর আমার প্রায়ই মনে হয়ে, নিজেদের চেয়ে দরিদ্র দেশে ঘুরে বেড়াতে আরাম বেশি। কিন্তু নেপালের কথা আলাদা। নেপাল ঘুরে আসার পর মনে হলো, ধনী দেশের মধ্যে পড়লেও ওই দেশ আমার অনেক আগেই ভালো করে দেখা উচিত ছিলো।বলতে দ্বিধা নেই, সৃষ্টিকর্তা অনেক বাড়াবাড়ি রকমের সৌন্দর্য দিয়ে তাঁকে লালন করছেন। পাহাড়, মেঘ আর সবুজের এমন সৌন্দর্য পৃথিবীর খুব কম লোকালয়ে আছে। সুইজারল্যান্ড বা সালযবুর্গে যা আছে, নেপালে তার কমতি নেই কোনো অর্থেই। শুধু তাই নয়, মধ্যযুগের দালান নির্মাণে যে পরিমাণ কারুশিল্প তাঁরা সৃষ্টি করেছিলো, সেগুলোও আজকের যুগের এক বিস্ময়। ২০১৪ সালের অক্টোবর মাসে বাংলাদেশ থেকে যাওয়া একদল স্থপতির সঙ্গে প্রায় বাধ্য হয়ে ৮ রাতের সফরে গিয়েছিলাম নেপাল। সে দলের সঙ্গে ঘুরলাম পোখরা, নাগরকোট আর কাঠমান্ডু অঞ্চল। যে স্থাপত্য সম্মেলনের নামে আমার যাওয়া ছিলো, তার রেজিস্ট্রেশন ছিলো না আমার, সে কারণে দিনভর ঝিমিয়ে ঝিমিয়ে বক্তৃতা শোনার সময়টা আমি কাটিয়েছি আমার ক্যামেরা যুগল নিয়ে। চোখ ভরে দেখেছি, কিছু মানুষের সঙ্গেও মিশেছি। সেসবের কাহিনি নিয়েই আমার এ ভ্রমণকথা অন্নপূর্ণায়। এ বইটিতে ব্যবহৃত ছবিগুলো বেশিরভাগই আমার তোলা, কিছু ছবি তুলেছে আমার সহ স্থপতি-পর্যটকেরা। কৃতজ্ঞতা তাঁদের প্রতিও।</span><br /><span>শাকুর মজিদ</span></p>', '', 'Jan 06, 2015', 'January 6, 2015, 3:41 am'),
(5, 1, 0, 0, 6, 'ভারতের অর্থনৈতিক ইতিহাস', 83, 0, 575.00, '', 575.00, '1425891474.gif', '', 'Available at publisher&#039;s end', '140', 52, '3rd', '', 'Bangla', '', '', 'Jan 06, 2015', 'January 6, 2015, 4:15 am'),
(6, 1, 0, 0, 1, 'নাইন্টিন সেভেন্টিওয়ান', 83, 0, 750.00, '', 750.00, '1425892699.GIF', '', 'Available at publisher&#039;s end', '140', 52, '3rd', '', 'Bangla', '', '', 'Jan 06, 2015', 'January 6, 2015, 4:15 am'),
(7, 1, 3, 0, 1, 'নাইন্টিন সেভেন্টিওয়ান', 32, 0, 1240.00, '25', 930.00, '1425892644.gif', '', 'Available at publisher&#039;s end', '500', 46, '২য়', '', 'Bangla', '<p><strong>বইয়ে যা আছে</strong><br /><br /><span>মুখবন্ধ: হাবিবুল বাশার/দেবব্রতর কৈফিয়ত/</span><br /><br /><span>সাকিবের সাকিব/</span><br /><span>আকাশ ছোঁয়ার স্বপ্ন দেখি: ২০০৯/</span><br /><span>এখনও অনেক পথ পাড়ি দিতে হবে: ২০১১/</span><br /><span>নিজেকে হারাতে চাই না: ২০১৪/</span><br /><span>তাহাদের সাকিব/</span><br /><span>আমার ফয়সাল আগের মতোই আছে: শিরিন আখতার; সাকিবের মা/</span><br /><span>নিজের স্বপ্ন সত্যি করেছে ফয়সাল: মাশরুর রেজা; সাকিবের বাবা/</span><br /><span>আমার ভাইয়া লুডুতেও সেরা: জান্নাতুল ফেরদৌস রিতু/</span><br /><span>ওকে নিয়ে আমার ভয় নেই: উম্মে আহমেদ শিশির/</span><br /><span>আমার আর কিচ্ছু চাওয়ার নেই: সাদ্দাম হোসেন গের্কি/</span><br /><span>আমরা ছিলাম চ্যাম্পিয়ন: সৈয়দ তারিক আনাম প্রতীক: সাকিবের বাল্যবন্ধু/</span><br /><span>একদিন যেন দেশকে সেরা করতে পারে: সালাউদ্দিন আহমেদ; কোচ/</span><br /><span>চ্যাম্পিয়নরা উদ্ধতই হয়: জেমি সিডন্স/</span><br /><span>সর্বকালের সেরাদের একজন হতে হবে: মাশরাফি বিন মুর্তজা/</span><br /><span>সত্যিকারের চ্যাম্পিয়ন ক্রিকেটার: তামিম ইকবাল/</span><br /><span>সাকিবের সঙ্গে এক দলে খেলাটা ভাগ্যের ব্যাপার: মুশফিকুর রহিম/</span><br /><span>সাকিবের মূল্যায়ন যথার্থ হয় না: আব্দুর রাজ্জাক/</span><br /><span>সে অলরাউন্ডার ক্লাবের যোগ্য সদস্য: জ্যাক ক্যালিস/</span><br /><span>সাকিব ভাই আমাদের প্রেরণা: সালমা খাতুন/</span><br /><span>আমিও সাবিবের ভক্ত: জাহিদ হোসেন এমিলি/</span><br /><span>চাকচিক্যে ভোলেনি সাকিব: নিয়াজ মোরশেদ/</span><br /><span>চাপটা সবচেয়ে ভালো সামলাতে পারে: আতহার আলী খান/</span><br /><span>বিস্ময়কর কমিটমেন্ট: সঞ্জয় মাঞ্জরেকার/</span><br /><span>সাকিবের যত্ন নিতে হবে: ওয়াসিম আকরাম/</span><br /><span>বাংলাদেশকে নতুন উচ্চতায় নেবে: সাকলাইন মুশতাক/</span><br /><span>মিডিয়া গেমটা খুব ভালো বোঝে: অ্যান্ডু মিলার/</span><br /><span>ভিনু মানকড়ের সঙ্গে তুলনা করতে চাই: শিল্ড বেরি/</span><br /><span>আমাদের আন্তর্জতিক পরিচয় সাকিব: আনিসুল হক/</span><br /><span>রোজকার দেখা চরিত্র নয় সাকির: উৎপল শুভ্র/</span><br /><span>সাকিবের এবং ক্রিকেটের মঙ্গল চাই: মোস্তফা মামুন/</span><br /><span>বাংলাদেশের সবচেয়ে ভুল বোঝো মানুষ: রাবিদ ইমাম/</span><br /><span>আমাদের সাকিব/</span><br /><span>মাগুরা থোক এভারেস্ট/ (সাকিবের সংক্ষিপ্ত বায়োগ্রাফি)</span><br /><span>এক নজরে সাকিব/</span></p>', '', 'Dec 13, 2014', 'Dec 13, 2014'),
(8, 1, 3, 0, 1, 'নাইন্টিন সেভেন্টিওয়ান', 31, 0, 1240.00, '25', 930.00, '1425892757.gif', '', 'Available at publisher&#039;s end', '500', 46, '২য়', '', 'Bangla', '<p><strong>বইয়ে যা আছে</strong><br /><br /><span>মুখবন্ধ: হাবিবুল বাশার/দেবব্রতর কৈফিয়ত/</span><br /><br /><span>সাকিবের সাকিব/</span><br /><span>আকাশ ছোঁয়ার স্বপ্ন দেখি: ২০০৯/</span><br /><span>এখনও অনেক পথ পাড়ি দিতে হবে: ২০১১/</span><br /><span>নিজেকে হারাতে চাই না: ২০১৪/</span><br /><span>তাহাদের সাকিব/</span><br /><span>আমার ফয়সাল আগের মতোই আছে: শিরিন আখতার; সাকিবের মা/</span><br /><span>নিজের স্বপ্ন সত্যি করেছে ফয়সাল: মাশরুর রেজা; সাকিবের বাবা/</span><br /><span>আমার ভাইয়া লুডুতেও সেরা: জান্নাতুল ফেরদৌস রিতু/</span><br /><span>ওকে নিয়ে আমার ভয় নেই: উম্মে আহমেদ শিশির/</span><br /><span>আমার আর কিচ্ছু চাওয়ার নেই: সাদ্দাম হোসেন গের্কি/</span><br /><span>আমরা ছিলাম চ্যাম্পিয়ন: সৈয়দ তারিক আনাম প্রতীক: সাকিবের বাল্যবন্ধু/</span><br /><span>একদিন যেন দেশকে সেরা করতে পারে: সালাউদ্দিন আহমেদ; কোচ/</span><br /><span>চ্যাম্পিয়নরা উদ্ধতই হয়: জেমি সিডন্স/</span><br /><span>সর্বকালের সেরাদের একজন হতে হবে: মাশরাফি বিন মুর্তজা/</span><br /><span>সত্যিকারের চ্যাম্পিয়ন ক্রিকেটার: তামিম ইকবাল/</span><br /><span>সাকিবের সঙ্গে এক দলে খেলাটা ভাগ্যের ব্যাপার: মুশফিকুর রহিম/</span><br /><span>সাকিবের মূল্যায়ন যথার্থ হয় না: আব্দুর রাজ্জাক/</span><br /><span>সে অলরাউন্ডার ক্লাবের যোগ্য সদস্য: জ্যাক ক্যালিস/</span><br /><span>সাকিব ভাই আমাদের প্রেরণা: সালমা খাতুন/</span><br /><span>আমিও সাবিবের ভক্ত: জাহিদ হোসেন এমিলি/</span><br /><span>চাকচিক্যে ভোলেনি সাকিব: নিয়াজ মোরশেদ/</span><br /><span>চাপটা সবচেয়ে ভালো সামলাতে পারে: আতহার আলী খান/</span><br /><span>বিস্ময়কর কমিটমেন্ট: সঞ্জয় মাঞ্জরেকার/</span><br /><span>সাকিবের যত্ন নিতে হবে: ওয়াসিম আকরাম/</span><br /><span>বাংলাদেশকে নতুন উচ্চতায় নেবে: সাকলাইন মুশতাক/</span><br /><span>মিডিয়া গেমটা খুব ভালো বোঝে: অ্যান্ডু মিলার/</span><br /><span>ভিনু মানকড়ের সঙ্গে তুলনা করতে চাই: শিল্ড বেরি/</span><br /><span>আমাদের আন্তর্জতিক পরিচয় সাকিব: আনিসুল হক/</span><br /><span>রোজকার দেখা চরিত্র নয় সাকির: উৎপল শুভ্র/</span><br /><span>সাকিবের এবং ক্রিকেটের মঙ্গল চাই: মোস্তফা মামুন/</span><br /><span>বাংলাদেশের সবচেয়ে ভুল বোঝো মানুষ: রাবিদ ইমাম/</span><br /><span>আমাদের সাকিব/</span><br /><span>মাগুরা থোক এভারেস্ট/ (সাকিবের সংক্ষিপ্ত বায়োগ্রাফি)</span><br /><span>এক নজরে সাকিব/</span></p>', '', 'Dec 13, 2014', 'Dec 13, 2014'),
(9, 1, 0, 0, 1, 'নাইন্টিন সেভেন্টিওয়ান', 83, 0, 750.00, '', 750.00, '1425892791.GIF', '', 'Available at publisher&#039;s end', '140', 52, '3rd', '', 'Bangla', '', '', 'Jan 06, 2015', 'January 6, 2015, 4:15 am'),
(10, 1, 3, 0, 1, 'নাইন্টিন সেভেন্টিওয়ান', 32, 0, 1240.00, '25', 930.00, '1420486505.gif', '', 'Available at publisher&#039;s end', '500', 46, '২য়', '', 'Bangla', '<p><strong>বইয়ে যা আছে</strong><br /><br /><span>মুখবন্ধ: হাবিবুল বাশার/দেবব্রতর কৈফিয়ত/</span><br /><br /><span>সাকিবের সাকিব/</span><br /><span>আকাশ ছোঁয়ার স্বপ্ন দেখি: ২০০৯/</span><br /><span>এখনও অনেক পথ পাড়ি দিতে হবে: ২০১১/</span><br /><span>নিজেকে হারাতে চাই না: ২০১৪/</span><br /><span>তাহাদের সাকিব/</span><br /><span>আমার ফয়সাল আগের মতোই আছে: শিরিন আখতার; সাকিবের মা/</span><br /><span>নিজের স্বপ্ন সত্যি করেছে ফয়সাল: মাশরুর রেজা; সাকিবের বাবা/</span><br /><span>আমার ভাইয়া লুডুতেও সেরা: জান্নাতুল ফেরদৌস রিতু/</span><br /><span>ওকে নিয়ে আমার ভয় নেই: উম্মে আহমেদ শিশির/</span><br /><span>আমার আর কিচ্ছু চাওয়ার নেই: সাদ্দাম হোসেন গের্কি/</span><br /><span>আমরা ছিলাম চ্যাম্পিয়ন: সৈয়দ তারিক আনাম প্রতীক: সাকিবের বাল্যবন্ধু/</span><br /><span>একদিন যেন দেশকে সেরা করতে পারে: সালাউদ্দিন আহমেদ; কোচ/</span><br /><span>চ্যাম্পিয়নরা উদ্ধতই হয়: জেমি সিডন্স/</span><br /><span>সর্বকালের সেরাদের একজন হতে হবে: মাশরাফি বিন মুর্তজা/</span><br /><span>সত্যিকারের চ্যাম্পিয়ন ক্রিকেটার: তামিম ইকবাল/</span><br /><span>সাকিবের সঙ্গে এক দলে খেলাটা ভাগ্যের ব্যাপার: মুশফিকুর রহিম/</span><br /><span>সাকিবের মূল্যায়ন যথার্থ হয় না: আব্দুর রাজ্জাক/</span><br /><span>সে অলরাউন্ডার ক্লাবের যোগ্য সদস্য: জ্যাক ক্যালিস/</span><br /><span>সাকিব ভাই আমাদের প্রেরণা: সালমা খাতুন/</span><br /><span>আমিও সাবিবের ভক্ত: জাহিদ হোসেন এমিলি/</span><br /><span>চাকচিক্যে ভোলেনি সাকিব: নিয়াজ মোরশেদ/</span><br /><span>চাপটা সবচেয়ে ভালো সামলাতে পারে: আতহার আলী খান/</span><br /><span>বিস্ময়কর কমিটমেন্ট: সঞ্জয় মাঞ্জরেকার/</span><br /><span>সাকিবের যত্ন নিতে হবে: ওয়াসিম আকরাম/</span><br /><span>বাংলাদেশকে নতুন উচ্চতায় নেবে: সাকলাইন মুশতাক/</span><br /><span>মিডিয়া গেমটা খুব ভালো বোঝে: অ্যান্ডু মিলার/</span><br /><span>ভিনু মানকড়ের সঙ্গে তুলনা করতে চাই: শিল্ড বেরি/</span><br /><span>আমাদের আন্তর্জতিক পরিচয় সাকিব: আনিসুল হক/</span><br /><span>রোজকার দেখা চরিত্র নয় সাকির: উৎপল শুভ্র/</span><br /><span>সাকিবের এবং ক্রিকেটের মঙ্গল চাই: মোস্তফা মামুন/</span><br /><span>বাংলাদেশের সবচেয়ে ভুল বোঝো মানুষ: রাবিদ ইমাম/</span><br /><span>আমাদের সাকিব/</span><br /><span>মাগুরা থোক এভারেস্ট/ (সাকিবের সংক্ষিপ্ত বায়োগ্রাফি)</span><br /><span>এক নজরে সাকিব/</span></p>', '', 'Dec 13, 2014', 'Dec 13, 2014'),
(11, 1, 3, 0, 1, 'নাইন্টিন সেভেন্টি Three', 31, 0, 1240.00, '25', 930.00, '1425892854.gif', '', 'Available at publisher&#039;s end', '500', 46, '২য়', '', 'Bangla', '<p><strong>বইয়ে যা আছে</strong><br /><br /><span>মুখবন্ধ: হাবিবুল বাশার/দেবব্রতর কৈফিয়ত/</span><br /><br /><span>সাকিবের সাকিব/</span><br /><span>আকাশ ছোঁয়ার স্বপ্ন দেখি: ২০০৯/</span><br /><span>এখনও অনেক পথ পাড়ি দিতে হবে: ২০১১/</span><br /><span>নিজেকে হারাতে চাই না: ২০১৪/</span><br /><span>তাহাদের সাকিব/</span><br /><span>আমার ফয়সাল আগের মতোই আছে: শিরিন আখতার; সাকিবের মা/</span><br /><span>নিজের স্বপ্ন সত্যি করেছে ফয়সাল: মাশরুর রেজা; সাকিবের বাবা/</span><br /><span>আমার ভাইয়া লুডুতেও সেরা: জান্নাতুল ফেরদৌস রিতু/</span><br /><span>ওকে নিয়ে আমার ভয় নেই: উম্মে আহমেদ শিশির/</span><br /><span>আমার আর কিচ্ছু চাওয়ার নেই: সাদ্দাম হোসেন গের্কি/</span><br /><span>আমরা ছিলাম চ্যাম্পিয়ন: সৈয়দ তারিক আনাম প্রতীক: সাকিবের বাল্যবন্ধু/</span><br /><span>একদিন যেন দেশকে সেরা করতে পারে: সালাউদ্দিন আহমেদ; কোচ/</span><br /><span>চ্যাম্পিয়নরা উদ্ধতই হয়: জেমি সিডন্স/</span><br /><span>সর্বকালের সেরাদের একজন হতে হবে: মাশরাফি বিন মুর্তজা/</span><br /><span>সত্যিকারের চ্যাম্পিয়ন ক্রিকেটার: তামিম ইকবাল/</span><br /><span>সাকিবের সঙ্গে এক দলে খেলাটা ভাগ্যের ব্যাপার: মুশফিকুর রহিম/</span><br /><span>সাকিবের মূল্যায়ন যথার্থ হয় না: আব্দুর রাজ্জাক/</span><br /><span>সে অলরাউন্ডার ক্লাবের যোগ্য সদস্য: জ্যাক ক্যালিস/</span><br /><span>সাকিব ভাই আমাদের প্রেরণা: সালমা খাতুন/</span><br /><span>আমিও সাবিবের ভক্ত: জাহিদ হোসেন এমিলি/</span><br /><span>চাকচিক্যে ভোলেনি সাকিব: নিয়াজ মোরশেদ/</span><br /><span>চাপটা সবচেয়ে ভালো সামলাতে পারে: আতহার আলী খান/</span><br /><span>বিস্ময়কর কমিটমেন্ট: সঞ্জয় মাঞ্জরেকার/</span><br /><span>সাকিবের যত্ন নিতে হবে: ওয়াসিম আকরাম/</span><br /><span>বাংলাদেশকে নতুন উচ্চতায় নেবে: সাকলাইন মুশতাক/</span><br /><span>মিডিয়া গেমটা খুব ভালো বোঝে: অ্যান্ডু মিলার/</span><br /><span>ভিনু মানকড়ের সঙ্গে তুলনা করতে চাই: শিল্ড বেরি/</span><br /><span>আমাদের আন্তর্জতিক পরিচয় সাকিব: আনিসুল হক/</span><br /><span>রোজকার দেখা চরিত্র নয় সাকির: উৎপল শুভ্র/</span><br /><span>সাকিবের এবং ক্রিকেটের মঙ্গল চাই: মোস্তফা মামুন/</span><br /><span>বাংলাদেশের সবচেয়ে ভুল বোঝো মানুষ: রাবিদ ইমাম/</span><br /><span>আমাদের সাকিব/</span><br /><span>মাগুরা থোক এভারেস্ট/ (সাকিবের সংক্ষিপ্ত বায়োগ্রাফি)</span><br /><span>এক নজরে সাকিব/</span></p>', '', 'Dec 13, 2014', 'Dec 13, 2014'),
(12, 0, 0, 0, 1, '', 0, 0, 0.00, '', 0.00, '1425892398.GIF', '', '', '', 0, '', 'Bangladesh', '', '', '', 'Mar 09, 2015', 'March 9, 2015, 9:13 pm'),
(13, 31, 31, 0, 2, 'Tin ekke tin', 31, 0, 300.00, '30', 210.00, '1433629560.gif', '', '12', '86', 46, '1st, 2014', 'Bangladesh', 'Bangla', '<p>gfjhjhjhjhjhjhfvkbjmvkmv<br />fghkopfgjhofgujhif<br />hlpfghkoioo<br />hkfgjhiogjfgfghiofghopgkgfgopfogopf<br />hoppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppfg&nbsp;</p>', '', 'Jun 07, 2015', 'June 7, 2015, 10:29 am'),
(14, 31, 31, 0, 3, 'Amar ami', 31, 0, 80.00, '10', 72.00, '1433629991.gif', '', '50', '78', 47, '2nd, 2015', 'Bangladesh', 'bangla', '<p>jhgjghjghjhgjgh<br />jhkjhkjhkhjk<br />ghjhgjghjghjghjghj<br />hgjhgjghjghjghjghjgh<br />hjjhkjhkjhkjhtdterterter&nbsp;</p>', '', 'Jun 07, 2015', 'June 7, 2015, 10:34 am'),
(15, 31, 31, 0, 3, 'Tumi r ami', 31, 0, 200.00, '20', 160.00, '1433630155.gif', '', '22', '150', 48, '3rd, 2015', 'Bangladesh', 'bangla & english', '<p>uuiohfduioghdfighdfojgo<br />gdfmgjdfgdfkjgkldgkldfjggggggg<br />dfjkihgjdkdfgkdfhgjkdfhgjkdfhgjkdfg<br />gfdjgndfkgkdfgkldfjgkj<br />gkodfjgiodfjiogjdfiogjdfioj<br />gdfkjgoidfjgiodfjgkjmcvklbmcvkb&nbsp;</p>', '', 'Jun 07, 2015', 'June 7, 2015, 10:37 am'),
(16, 1, 1, 1, 3, 'Hello Test Book', 78, 0, 350.00, '10', 315.00, '1442465401.jpg', '', 'In stock', '50', 46, '2015', '', 'Bangla', '<p>Hello test book details.</p>', '', 'Jun 15, 2015', 'June 15, 2015, 4:40 am'),
(17, 2, 2, 17, 24, 'Test Product', 33, 0, 4000.00, '10', 3600.00, '1442468245.png', '', 'Available in stock', '200', 46, '2015', 'Bangladesh', 'Bangla', '<p>Test boook summery</p>', '', 'Sep 17, 2015', 'September 17, 2015, 7:37 pm'),
(21, 17, 92, 22, 12, 'Full tishert', 0, 0, 200.00, '', 0.00, '6174.jpg', '', '', '', 0, '', '', '', '<p>men&#39;s time</p>\r\n', '', '', ''),
(22, 17, 92, 26, 26, 'polo tshirt', 0, 0, 300.00, '', 0.00, '3012.jpg', '', '', '', 0, '', '', '', '<p>Nice polo tshirt red color, good quality</p>\r\n', '', '', ''),
(23, 17, 92, 25, 26, 'polo tshirt', 0, 0, 320.00, '', 0.00, '8685.jpg', '', '', '', 0, '', '', '', '<p>Nice polo tshirt red color, good quality</p>\r\n', '', '', ''),
(25, 17, 92, 23, 26, 'polo tshirt', 0, 0, 320.00, '', 0.00, '1922.jpg', '', '', '', 0, '', '', '', '<p>Nice polo tshirt red color, good quality</p>\r\n', '', '', ''),
(26, 22, 96, 24, 26, 'Jamdani Shari', 0, 0, 12000.00, '', 0.00, '1446.jpg', '', '', '', 0, '', '', '', '<p>Jmadani Shari Jmadani Shari Jmadani Shari&nbsp; Jmadani Shari Jmadani Shari</p>\r\n', '', '', ''),
(27, 22, 3, 27, 26, 'Sik Jamdani', 0, 20, 6000.00, '', 100.00, '', '', '', '', 0, '', '', '', '<p>Silk shari Silk shari Silk shari Silk shari Silk shari Silk shari Silk shari Silk shari Silk shari Silk shari Silk shari Silk shari</p>\r\n', 'No ', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `root_cat_id` int(12) NOT NULL,
  `cat_name` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` varchar(100) NOT NULL,
  `date_time` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `root_cat_id`, `cat_name`, `status`, `date`, `date_time`) VALUES
(1, 1, 'অনুবাদ', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(2, 1, 'আত্মউন্নয়ন', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(3, 22, 'KKKK', 'No', 'Dec 05, 2014', 'Dec 05, 2014'),
(4, 1, 'ইতিহাস, রাজনীতি ও সমাজ', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(5, 1, 'ইসলামী বই', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(6, 1, 'কমিকস ও নকশা', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(7, 1, 'কম্পিউটার ও ইন্টারনেট', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(8, 1, 'কৃষি ও কৃষক', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(9, 1, 'জীবনী ও স্মৃতিচারণ', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(10, 1, 'দর্শন', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(11, 1, 'স্কুল ও কলেজ বই', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(12, 1, 'ধর্মীয় বই', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(13, 1, 'পরিবার ও সম্পর্ক', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(14, 1, 'পরিবেশ ও প্রকৃতি', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(15, 1, 'প্রবন্ধ', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(16, 1, 'বইমেলা ২০১৪', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(17, 1, 'বাংলাদেশ', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(18, 1, 'বিজ্ঞান ও প্রযুক্তি', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(19, 1, 'ব্যবসা ও বিনিয়োগ', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(20, 1, 'ভাষা ও অভিধান', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(21, 1, 'ভ্রমন', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(22, 1, 'মুক্তিযুদ্ধ', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(23, 1, 'ম্যাগাজিন', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(24, 1, 'রচনাসমগ্র', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(25, 1, 'রান্নাবান্না, খাদ্য ও পুষ্টি', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(26, 1, 'শিক্ষা ও সহায়িকা', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(27, 1, 'শিশু ও কিশোর', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(28, 1, 'সংগীত, চলচ্চিত্র ও বিনোদন', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(29, 1, 'সাহিত্য', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(30, 1, 'স্বাস্হ্য ও পরিচর্যা', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(31, 1, 'à¦¹à¦¾à¦¸à¦¾à¦¨ à¦†à¦œà¦¿à¦œà§à¦² à¦¹à¦•', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(32, 1, 'à¦¹à¦¾à¦¸à¦¾à¦¨ à¦†à¦œà¦¿à¦œà§à¦² à¦¹à¦•', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(33, 17, 'à¦¹à¦¾à¦¸à¦¾à¦¨ à¦†à¦œà¦¿à¦œà§à¦² à¦¹à¦•', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(34, 1, 'à¦¹à¦¾à¦¸à¦¾à¦¨ à¦†à¦œà¦¿à¦œà§à¦² à¦¹à¦•', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(35, 1, 'à¦¸à¦²à¦¿à¦®à§à¦²à§à¦²à¦¾à¦¹ à¦–à¦¾à¦¨', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(36, 1, 'à¦¸à¦²à¦¿à¦®à§à¦²à§à¦²à¦¾à¦¹ à¦–à¦¾à¦¨', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(37, 2, 'à¦†à¦¹à¦¸à¦¾à¦¨ à¦¹à¦¾à¦¬à§€à¦¬', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(96, 22, 'Jamdani', 'Yes', '2016-05-25', '2016-05-25 08:09:20'),
(40, 2, 'আহসান হাবীব', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(41, 2, 'সরদার ফজলুল করিম', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(42, 2, 'আহমদ শরীফ', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(43, 2, 'মুনতাসীর মামুন', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(44, 2, 'সলিমুল্লাহ খান', 'Yes', 'Dec 05, 2014', 'Dec 05, 2014'),
(95, 17, 'Publishers', 'Yes', '2016-05-25', '2016-05-25 05:53:02'),
(46, 3, 'অন্যপ্রকাশ', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(47, 3, 'অনন্যা', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(48, 3, 'আগামী প্রকাশনী', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(49, 3, 'অ্যাডর্ন পাবলিকেশন', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(50, 3, 'ঐতিহ্য', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(51, 3, 'মাওলা ব্রাদার্স', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(52, 3, 'মুক্তধারা', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(53, 3, 'পাঠক সমাবেশ', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(54, 3, 'পার্ল পাবলিকেশন্স', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(55, 3, 'প্রথমা প্রকাশন', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(56, 3, 'সেবা প্রকাশনী', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(57, 3, 'পাঞ্জেরী পাবলিকেশন', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(58, 3, 'সময় প্রকাশন', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(59, 3, 'বাংলা একাডেমী', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(60, 3, 'অঙ্কুর প্রকাশনী', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(61, 3, 'অন্বেষা প্রকাশন', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(62, 3, 'ইত্যাদি গ্রন্থপ্রকাশ', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(63, 3, 'কাকলী প্রকাশনী', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(64, 3, 'মিজান পাবলিশার্স', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(65, 3, 'জ্ঞানকোষ প্রকাশনী', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(66, 3, 'দি ইউনিভার্সিটি প্রেস লিমিটেড', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(67, 3, 'বিশ্বসাহিত্য কেন্দ্র', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(68, 3, 'আনন্দ পাবলিশার্স', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(69, 3, 'দে’জ পাবলিশিং', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(70, 3, 'আধুনিক বই পাবলিকেশন', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(71, 3, 'ইসলামিক ফাউন্ডেশন', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(72, 3, 'সোলেমানিয়া বুক হাউস', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(73, 3, 'মদিনা পাবলিকেশন্স', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(74, 3, 'খোশরোজ কিতাবমহল', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(75, 3, 'এমদাদিয়া লাইব্রেরী', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(76, 2, 'হুমায়ুন আজাদ', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(77, 2, 'সৈয়দ শামসুল হক', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(78, 2, 'আহমদ ছফা', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(79, 2, 'আখতারুজ্জামান ইলিয়াস', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(80, 2, 'হাসান আজিজুল হক', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(81, 2, 'ফরহাদ মজহার', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(82, 1, 'বদরুদ্দীন উমর', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(83, 2, 'ড্যান ব্রাউন', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(84, 2, 'উইলিয়াম শেকসপীয়ার', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(85, 2, 'কাজী নজরুল ইসলাম', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(86, 2, 'রবীন্দ্রনাথ ঠাকুর', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(87, 2, 'সত্যজিৎ রায়', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(88, 2, 'সুনীল গঙ্গোপাধ্যায়', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(89, 2, 'শীর্ষেন্দু মুখোপাধ্যায়', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(90, 2, 'সমরেশ মজুমদার', 'Yes', 'Dec 06, 2014', 'Dec 06, 2014'),
(91, 3, 'সংগ্রহশালা প্রকাশনী', 'Yes', 'Mar 09, 2015', 'March 9, 2015, 9:04 pm'),
(92, 17, 'Tshirts', 'Yes', '2016-05-19', '2016-05-19 11:31:29'),
(93, 24, 'VShert', 'Yes', '2016-05-23', '2016-05-23 11:45:58'),
(94, 31, ' 	à¦¹à§à¦®à¦¾à§Ÿà§à¦¨ à¦†à¦¹à¦®à§‡à¦¦', 'Yes', '2016-05-24', '2016-05-24 11:18:08'),
(97, 27, 'Miniket', 'Yes', '2016-05-29', '2016-05-29 13:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `code` char(2) NOT NULL COMMENT 'Two-letter country code (ISO 3166-1 alpha-2)',
  `name` varchar(255) NOT NULL COMMENT 'English country name',
  `full_name` varchar(255) NOT NULL COMMENT 'Full English country name',
  `iso3` char(3) NOT NULL COMMENT 'Three-letter country code (ISO 3166-1 alpha-3)',
  `number` smallint(3) unsigned zerofill NOT NULL COMMENT 'Three-digit country number (ISO 3166-1 numeric)',
  `continent_code` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`code`, `name`, `full_name`, `iso3`, `number`, `continent_code`) VALUES
('AD', 'Andorra', 'Principality of Andorra', 'AND', 020, 'EU'),
('AE', 'United Arab Emirates', 'United Arab Emirates', 'ARE', 784, 'AS'),
('AF', 'Afghanistan', 'Islamic Republic of Afghanistan', 'AFG', 004, 'AS'),
('AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'ATG', 028, 'NA'),
('AI', 'Anguilla', 'Anguilla', 'AIA', 660, 'NA'),
('AL', 'Albania', 'Republic of Albania', 'ALB', 008, 'EU'),
('AM', 'Armenia', 'Republic of Armenia', 'ARM', 051, 'AS'),
('AO', 'Angola', 'Republic of Angola', 'AGO', 024, 'AF'),
('AQ', 'Antarctica', 'Antarctica (the territory South of 60 deg S)', 'ATA', 010, 'AN'),
('AR', 'Argentina', 'Argentine Republic', 'ARG', 032, 'SA'),
('AS', 'American Samoa', 'American Samoa', 'ASM', 016, 'OC'),
('AT', 'Austria', 'Republic of Austria', 'AUT', 040, 'EU'),
('AU', 'Australia', 'Commonwealth of Australia', 'AUS', 036, 'OC'),
('AW', 'Aruba', 'Aruba', 'ABW', 533, 'NA'),
('AX', 'Åland Islands', 'Åland Islands', 'ALA', 248, 'EU'),
('AZ', 'Azerbaijan', 'Republic of Azerbaijan', 'AZE', 031, 'AS'),
('BA', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BIH', 070, 'EU'),
('BB', 'Barbados', 'Barbados', 'BRB', 052, 'NA'),
('BD', 'Bangladesh', 'People''s Republic of Bangladesh', 'BGD', 050, 'AS'),
('BE', 'Belgium', 'Kingdom of Belgium', 'BEL', 056, 'EU'),
('BF', 'Burkina Faso', 'Burkina Faso', 'BFA', 854, 'AF'),
('BG', 'Bulgaria', 'Republic of Bulgaria', 'BGR', 100, 'EU'),
('BH', 'Bahrain', 'Kingdom of Bahrain', 'BHR', 048, 'AS'),
('BI', 'Burundi', 'Republic of Burundi', 'BDI', 108, 'AF'),
('BJ', 'Benin', 'Republic of Benin', 'BEN', 204, 'AF'),
('BL', 'Saint Barthélemy', 'Saint Barthélemy', 'BLM', 652, 'NA'),
('BM', 'Bermuda', 'Bermuda', 'BMU', 060, 'NA'),
('BN', 'Brunei Darussalam', 'Brunei Darussalam', 'BRN', 096, 'AS'),
('BO', 'Bolivia', 'Plurinational State of Bolivia', 'BOL', 068, 'SA'),
('BQ', 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BES', 535, 'NA'),
('BR', 'Brazil', 'Federative Republic of Brazil', 'BRA', 076, 'SA'),
('BS', 'Bahamas', 'Commonwealth of the Bahamas', 'BHS', 044, 'NA'),
('BT', 'Bhutan', 'Kingdom of Bhutan', 'BTN', 064, 'AS'),
('BV', 'Bouvet Island (Bouvetoya)', 'Bouvet Island (Bouvetoya)', 'BVT', 074, 'AN'),
('BW', 'Botswana', 'Republic of Botswana', 'BWA', 072, 'AF'),
('BY', 'Belarus', 'Republic of Belarus', 'BLR', 112, 'EU'),
('BZ', 'Belize', 'Belize', 'BLZ', 084, 'NA'),
('CA', 'Canada', 'Canada', 'CAN', 124, 'NA'),
('CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CCK', 166, 'AS'),
('CD', 'Congo', 'Democratic Republic of the Congo', 'COD', 180, 'AF'),
('CF', 'Central African Republic', 'Central African Republic', 'CAF', 140, 'AF'),
('CG', 'Congo', 'Republic of the Congo', 'COG', 178, 'AF'),
('CH', 'Switzerland', 'Swiss Confederation', 'CHE', 756, 'EU'),
('CI', 'Cote d''Ivoire', 'Republic of Cote d''Ivoire', 'CIV', 384, 'AF'),
('CK', 'Cook Islands', 'Cook Islands', 'COK', 184, 'OC'),
('CL', 'Chile', 'Republic of Chile', 'CHL', 152, 'SA'),
('CM', 'Cameroon', 'Republic of Cameroon', 'CMR', 120, 'AF'),
('CN', 'China', 'People''s Republic of China', 'CHN', 156, 'AS'),
('CO', 'Colombia', 'Republic of Colombia', 'COL', 170, 'SA'),
('CR', 'Costa Rica', 'Republic of Costa Rica', 'CRI', 188, 'NA'),
('CU', 'Cuba', 'Republic of Cuba', 'CUB', 192, 'NA'),
('CV', 'Cape Verde', 'Republic of Cape Verde', 'CPV', 132, 'AF'),
('CW', 'Curaçao', 'Curaçao', 'CUW', 531, 'NA'),
('CX', 'Christmas Island', 'Christmas Island', 'CXR', 162, 'AS'),
('CY', 'Cyprus', 'Republic of Cyprus', 'CYP', 196, 'AS'),
('CZ', 'Czech Republic', 'Czech Republic', 'CZE', 203, 'EU'),
('DE', 'Germany', 'Federal Republic of Germany', 'DEU', 276, 'EU'),
('DJ', 'Djibouti', 'Republic of Djibouti', 'DJI', 262, 'AF'),
('DK', 'Denmark', 'Kingdom of Denmark', 'DNK', 208, 'EU'),
('DM', 'Dominica', 'Commonwealth of Dominica', 'DMA', 212, 'NA'),
('DO', 'Dominican Republic', 'Dominican Republic', 'DOM', 214, 'NA'),
('DZ', 'Algeria', 'People''s Democratic Republic of Algeria', 'DZA', 012, 'AF'),
('EC', 'Ecuador', 'Republic of Ecuador', 'ECU', 218, 'SA'),
('EE', 'Estonia', 'Republic of Estonia', 'EST', 233, 'EU'),
('EG', 'Egypt', 'Arab Republic of Egypt', 'EGY', 818, 'AF'),
('EH', 'Western Sahara', 'Western Sahara', 'ESH', 732, 'AF'),
('ER', 'Eritrea', 'State of Eritrea', 'ERI', 232, 'AF'),
('ES', 'Spain', 'Kingdom of Spain', 'ESP', 724, 'EU'),
('ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ETH', 231, 'AF'),
('FI', 'Finland', 'Republic of Finland', 'FIN', 246, 'EU'),
('FJ', 'Fiji', 'Republic of Fiji', 'FJI', 242, 'OC'),
('FK', 'Falkland Islands (Malvinas)', 'Falkland Islands (Malvinas)', 'FLK', 238, 'SA'),
('FM', 'Micronesia', 'Federated States of Micronesia', 'FSM', 583, 'OC'),
('FO', 'Faroe Islands', 'Faroe Islands', 'FRO', 234, 'EU'),
('FR', 'France', 'French Republic', 'FRA', 250, 'EU'),
('GA', 'Gabon', 'Gabonese Republic', 'GAB', 266, 'AF'),
('GB', 'United Kingdom of Great Britain & Northern Ireland', 'United Kingdom of Great Britain & Northern Ireland', 'GBR', 826, 'EU'),
('GD', 'Grenada', 'Grenada', 'GRD', 308, 'NA'),
('GE', 'Georgia', 'Georgia', 'GEO', 268, 'AS'),
('GF', 'French Guiana', 'French Guiana', 'GUF', 254, 'SA'),
('GG', 'Guernsey', 'Bailiwick of Guernsey', 'GGY', 831, 'EU'),
('GH', 'Ghana', 'Republic of Ghana', 'GHA', 288, 'AF'),
('GI', 'Gibraltar', 'Gibraltar', 'GIB', 292, 'EU'),
('GL', 'Greenland', 'Greenland', 'GRL', 304, 'NA'),
('GM', 'Gambia', 'Republic of the Gambia', 'GMB', 270, 'AF'),
('GN', 'Guinea', 'Republic of Guinea', 'GIN', 324, 'AF'),
('GP', 'Guadeloupe', 'Guadeloupe', 'GLP', 312, 'NA'),
('GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GNQ', 226, 'AF'),
('GR', 'Greece', 'Hellenic Republic Greece', 'GRC', 300, 'EU'),
('GS', 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'SGS', 239, 'AN'),
('GT', 'Guatemala', 'Republic of Guatemala', 'GTM', 320, 'NA'),
('GU', 'Guam', 'Guam', 'GUM', 316, 'OC'),
('GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GNB', 624, 'AF'),
('GY', 'Guyana', 'Co-operative Republic of Guyana', 'GUY', 328, 'SA'),
('HK', 'Hong Kong', 'Hong Kong Special Administrative Region of China', 'HKG', 344, 'AS'),
('HM', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HMD', 334, 'AN'),
('HN', 'Honduras', 'Republic of Honduras', 'HND', 340, 'NA'),
('HR', 'Croatia', 'Republic of Croatia', 'HRV', 191, 'EU'),
('HT', 'Haiti', 'Republic of Haiti', 'HTI', 332, 'NA'),
('HU', 'Hungary', 'Hungary', 'HUN', 348, 'EU'),
('ID', 'Indonesia', 'Republic of Indonesia', 'IDN', 360, 'AS'),
('IE', 'Ireland', 'Ireland', 'IRL', 372, 'EU'),
('IL', 'Israel', 'State of Israel', 'ISR', 376, 'AS'),
('IM', 'Isle of Man', 'Isle of Man', 'IMN', 833, 'EU'),
('IN', 'India', 'Republic of India', 'IND', 356, 'AS'),
('IO', 'British Indian Ocean Territory (Chagos Archipelago)', 'British Indian Ocean Territory (Chagos Archipelago)', 'IOT', 086, 'AS'),
('IQ', 'Iraq', 'Republic of Iraq', 'IRQ', 368, 'AS'),
('IR', 'Iran', 'Islamic Republic of Iran', 'IRN', 364, 'AS'),
('IS', 'Iceland', 'Republic of Iceland', 'ISL', 352, 'EU'),
('IT', 'Italy', 'Italian Republic', 'ITA', 380, 'EU'),
('JE', 'Jersey', 'Bailiwick of Jersey', 'JEY', 832, 'EU'),
('JM', 'Jamaica', 'Jamaica', 'JAM', 388, 'NA'),
('JO', 'Jordan', 'Hashemite Kingdom of Jordan', 'JOR', 400, 'AS'),
('JP', 'Japan', 'Japan', 'JPN', 392, 'AS'),
('KE', 'Kenya', 'Republic of Kenya', 'KEN', 404, 'AF'),
('KG', 'Kyrgyz Republic', 'Kyrgyz Republic', 'KGZ', 417, 'AS'),
('KH', 'Cambodia', 'Kingdom of Cambodia', 'KHM', 116, 'AS'),
('KI', 'Kiribati', 'Republic of Kiribati', 'KIR', 296, 'OC'),
('KM', 'Comoros', 'Union of the Comoros', 'COM', 174, 'AF'),
('KN', 'Saint Kitts and Nevis', 'Federation of Saint Kitts and Nevis', 'KNA', 659, 'NA'),
('KP', 'Korea', 'Democratic People''s Republic of Korea', 'PRK', 408, 'AS'),
('KR', 'Korea', 'Republic of Korea', 'KOR', 410, 'AS'),
('KW', 'Kuwait', 'State of Kuwait', 'KWT', 414, 'AS'),
('KY', 'Cayman Islands', 'Cayman Islands', 'CYM', 136, 'NA'),
('KZ', 'Kazakhstan', 'Republic of Kazakhstan', 'KAZ', 398, 'AS'),
('LA', 'Lao People''s Democratic Republic', 'Lao People''s Democratic Republic', 'LAO', 418, 'AS'),
('LB', 'Lebanon', 'Lebanese Republic', 'LBN', 422, 'AS'),
('LC', 'Saint Lucia', 'Saint Lucia', 'LCA', 662, 'NA'),
('LI', 'Liechtenstein', 'Principality of Liechtenstein', 'LIE', 438, 'EU'),
('LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LKA', 144, 'AS'),
('LR', 'Liberia', 'Republic of Liberia', 'LBR', 430, 'AF'),
('LS', 'Lesotho', 'Kingdom of Lesotho', 'LSO', 426, 'AF'),
('LT', 'Lithuania', 'Republic of Lithuania', 'LTU', 440, 'EU'),
('LU', 'Luxembourg', 'Grand Duchy of Luxembourg', 'LUX', 442, 'EU'),
('LV', 'Latvia', 'Republic of Latvia', 'LVA', 428, 'EU'),
('LY', 'Libya', 'Libya', 'LBY', 434, 'AF'),
('MA', 'Morocco', 'Kingdom of Morocco', 'MAR', 504, 'AF'),
('MC', 'Monaco', 'Principality of Monaco', 'MCO', 492, 'EU'),
('MD', 'Moldova', 'Republic of Moldova', 'MDA', 498, 'EU'),
('ME', 'Montenegro', 'Montenegro', 'MNE', 499, 'EU'),
('MF', 'Saint Martin', 'Saint Martin (French part)', 'MAF', 663, 'NA'),
('MG', 'Madagascar', 'Republic of Madagascar', 'MDG', 450, 'AF'),
('MH', 'Marshall Islands', 'Republic of the Marshall Islands', 'MHL', 584, 'OC'),
('MK', 'Macedonia', 'Republic of Macedonia', 'MKD', 807, 'EU'),
('ML', 'Mali', 'Republic of Mali', 'MLI', 466, 'AF'),
('MM', 'Myanmar', 'Republic of the Union of Myanmar', 'MMR', 104, 'AS'),
('MN', 'Mongolia', 'Mongolia', 'MNG', 496, 'AS'),
('MO', 'Macao', 'Macao Special Administrative Region of China', 'MAC', 446, 'AS'),
('MP', 'Northern Mariana Islands', 'Commonwealth of the Northern Mariana Islands', 'MNP', 580, 'OC'),
('MQ', 'Martinique', 'Martinique', 'MTQ', 474, 'NA'),
('MR', 'Mauritania', 'Islamic Republic of Mauritania', 'MRT', 478, 'AF'),
('MS', 'Montserrat', 'Montserrat', 'MSR', 500, 'NA'),
('MT', 'Malta', 'Republic of Malta', 'MLT', 470, 'EU'),
('MU', 'Mauritius', 'Republic of Mauritius', 'MUS', 480, 'AF'),
('MV', 'Maldives', 'Republic of Maldives', 'MDV', 462, 'AS'),
('MW', 'Malawi', 'Republic of Malawi', 'MWI', 454, 'AF'),
('MX', 'Mexico', 'United Mexican States', 'MEX', 484, 'NA'),
('MY', 'Malaysia', 'Malaysia', 'MYS', 458, 'AS'),
('MZ', 'Mozambique', 'Republic of Mozambique', 'MOZ', 508, 'AF'),
('NA', 'Namibia', 'Republic of Namibia', 'NAM', 516, 'AF'),
('NC', 'New Caledonia', 'New Caledonia', 'NCL', 540, 'OC'),
('NE', 'Niger', 'Republic of Niger', 'NER', 562, 'AF'),
('NF', 'Norfolk Island', 'Norfolk Island', 'NFK', 574, 'OC'),
('NG', 'Nigeria', 'Federal Republic of Nigeria', 'NGA', 566, 'AF'),
('NI', 'Nicaragua', 'Republic of Nicaragua', 'NIC', 558, 'NA'),
('NL', 'Netherlands', 'Kingdom of the Netherlands', 'NLD', 528, 'EU'),
('NO', 'Norway', 'Kingdom of Norway', 'NOR', 578, 'EU'),
('NP', 'Nepal', 'Federal Democratic Republic of Nepal', 'NPL', 524, 'AS'),
('NR', 'Nauru', 'Republic of Nauru', 'NRU', 520, 'OC'),
('NU', 'Niue', 'Niue', 'NIU', 570, 'OC'),
('NZ', 'New Zealand', 'New Zealand', 'NZL', 554, 'OC'),
('OM', 'Oman', 'Sultanate of Oman', 'OMN', 512, 'AS'),
('PA', 'Panama', 'Republic of Panama', 'PAN', 591, 'NA'),
('PE', 'Peru', 'Republic of Peru', 'PER', 604, 'SA'),
('PF', 'French Polynesia', 'French Polynesia', 'PYF', 258, 'OC'),
('PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PNG', 598, 'OC'),
('PH', 'Philippines', 'Republic of the Philippines', 'PHL', 608, 'AS'),
('PK', 'Pakistan', 'Islamic Republic of Pakistan', 'PAK', 586, 'AS'),
('PL', 'Poland', 'Republic of Poland', 'POL', 616, 'EU'),
('PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'SPM', 666, 'NA'),
('PN', 'Pitcairn Islands', 'Pitcairn Islands', 'PCN', 612, 'OC'),
('PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PRI', 630, 'NA'),
('PS', 'Palestinian Territory', 'Occupied Palestinian Territory', 'PSE', 275, 'AS'),
('PT', 'Portugal', 'Portuguese Republic', 'PRT', 620, 'EU'),
('PW', 'Palau', 'Republic of Palau', 'PLW', 585, 'OC'),
('PY', 'Paraguay', 'Republic of Paraguay', 'PRY', 600, 'SA'),
('QA', 'Qatar', 'State of Qatar', 'QAT', 634, 'AS'),
('RE', 'Réunion', 'Réunion', 'REU', 638, 'AF'),
('RO', 'Romania', 'Romania', 'ROU', 642, 'EU'),
('RS', 'Serbia', 'Republic of Serbia', 'SRB', 688, 'EU'),
('RU', 'Russian Federation', 'Russian Federation', 'RUS', 643, 'EU'),
('RW', 'Rwanda', 'Republic of Rwanda', 'RWA', 646, 'AF'),
('SA', 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'SAU', 682, 'AS'),
('SB', 'Solomon Islands', 'Solomon Islands', 'SLB', 090, 'OC'),
('SC', 'Seychelles', 'Republic of Seychelles', 'SYC', 690, 'AF'),
('SD', 'Sudan', 'Republic of Sudan', 'SDN', 729, 'AF'),
('SE', 'Sweden', 'Kingdom of Sweden', 'SWE', 752, 'EU'),
('SG', 'Singapore', 'Republic of Singapore', 'SGP', 702, 'AS'),
('SH', 'Saint Helena, Ascension and Tristan da Cunha', 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', 654, 'AF'),
('SI', 'Slovenia', 'Republic of Slovenia', 'SVN', 705, 'EU'),
('SJ', 'Svalbard & Jan Mayen Islands', 'Svalbard & Jan Mayen Islands', 'SJM', 744, 'EU'),
('SK', 'Slovakia (Slovak Republic)', 'Slovakia (Slovak Republic)', 'SVK', 703, 'EU'),
('SL', 'Sierra Leone', 'Republic of Sierra Leone', 'SLE', 694, 'AF'),
('SM', 'San Marino', 'Republic of San Marino', 'SMR', 674, 'EU'),
('SN', 'Senegal', 'Republic of Senegal', 'SEN', 686, 'AF'),
('SO', 'Somalia', 'Somali Republic', 'SOM', 706, 'AF'),
('SR', 'Suriname', 'Republic of Suriname', 'SUR', 740, 'SA'),
('SS', 'South Sudan', 'Republic of South Sudan', 'SSD', 728, 'AF'),
('ST', 'Sao Tome and Principe', 'Democratic Republic of Sao Tome and Principe', 'STP', 678, 'AF'),
('SV', 'El Salvador', 'Republic of El Salvador', 'SLV', 222, 'NA'),
('SX', 'Sint Maarten (Dutch part)', 'Sint Maarten (Dutch part)', 'SXM', 534, 'NA'),
('SY', 'Syrian Arab Republic', 'Syrian Arab Republic', 'SYR', 760, 'AS'),
('SZ', 'Swaziland', 'Kingdom of Swaziland', 'SWZ', 748, 'AF'),
('TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TCA', 796, 'NA'),
('TD', 'Chad', 'Republic of Chad', 'TCD', 148, 'AF'),
('TF', 'French Southern Territories', 'French Southern Territories', 'ATF', 260, 'AN'),
('TG', 'Togo', 'Togolese Republic', 'TGO', 768, 'AF'),
('TH', 'Thailand', 'Kingdom of Thailand', 'THA', 764, 'AS'),
('TJ', 'Tajikistan', 'Republic of Tajikistan', 'TJK', 762, 'AS'),
('TK', 'Tokelau', 'Tokelau', 'TKL', 772, 'OC'),
('TL', 'Timor-Leste', 'Democratic Republic of Timor-Leste', 'TLS', 626, 'AS'),
('TM', 'Turkmenistan', 'Turkmenistan', 'TKM', 795, 'AS'),
('TN', 'Tunisia', 'Tunisian Republic', 'TUN', 788, 'AF'),
('TO', 'Tonga', 'Kingdom of Tonga', 'TON', 776, 'OC'),
('TR', 'Turkey', 'Republic of Turkey', 'TUR', 792, 'AS'),
('TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TTO', 780, 'NA'),
('TV', 'Tuvalu', 'Tuvalu', 'TUV', 798, 'OC'),
('TW', 'Taiwan', 'Taiwan, Province of China', 'TWN', 158, 'AS'),
('TZ', 'Tanzania', 'United Republic of Tanzania', 'TZA', 834, 'AF'),
('UA', 'Ukraine', 'Ukraine', 'UKR', 804, 'EU'),
('UG', 'Uganda', 'Republic of Uganda', 'UGA', 800, 'AF'),
('UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UMI', 581, 'OC'),
('US', 'United States of America', 'United States of America', 'USA', 840, 'NA'),
('UY', 'Uruguay', 'Eastern Republic of Uruguay', 'URY', 858, 'SA'),
('UZ', 'Uzbekistan', 'Republic of Uzbekistan', 'UZB', 860, 'AS'),
('VA', 'Holy See (Vatican City State)', 'Holy See (Vatican City State)', 'VAT', 336, 'EU'),
('VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VCT', 670, 'NA'),
('VE', 'Venezuela', 'Bolivarian Republic of Venezuela', 'VEN', 862, 'SA'),
('VG', 'British Virgin Islands', 'British Virgin Islands', 'VGB', 092, 'NA'),
('VI', 'United States Virgin Islands', 'United States Virgin Islands', 'VIR', 850, 'NA'),
('VN', 'Vietnam', 'Socialist Republic of Vietnam', 'VNM', 704, 'AS'),
('VU', 'Vanuatu', 'Republic of Vanuatu', 'VUT', 548, 'OC'),
('WF', 'Wallis and Futuna', 'Wallis and Futuna', 'WLF', 876, 'OC'),
('WS', 'Samoa', 'Independent State of Samoa', 'WSM', 882, 'OC'),
('YE', 'Yemen', 'Yemen', 'YEM', 887, 'AS'),
('YT', 'Mayotte', 'Mayotte', 'MYT', 175, 'AF'),
('ZA', 'South Africa', 'Republic of South Africa', 'ZAF', 710, 'AF'),
('ZM', 'Zambia', 'Republic of Zambia', 'ZMB', 894, 'AF'),
('ZW', 'Zimbabwe', 'Republic of Zimbabwe', 'ZWE', 716, 'AF');

-- --------------------------------------------------------

--
-- Table structure for table `image_product`
--

CREATE TABLE IF NOT EXISTS `image_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `photo` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image_product`
--

INSERT INTO `image_product` (`id`, `product_id`, `photo`, `status`) VALUES
(1, 21, '6174.jpg', 0),
(2, 21, '5099.jpg', 0),
(3, 21, '3892.jpg', 0),
(4, 22, '8484.jpg', 0),
(5, 22, '3012.jpg', 0),
(6, 22, '4788.jpg', 0),
(7, 23, '2449.jpg', 0),
(8, 23, '1628.jpg', 0),
(9, 23, '4764.jpg', 0),
(10, 24, '8685.jpg', 0),
(11, 24, '3117.jpg', 0),
(12, 24, '3810.jpg', 0),
(13, 25, '1922.jpg', 0),
(14, 25, '4257.jpg', 0),
(15, 25, '2004.jpg', 0),
(16, 26, '7384.jpg', 0),
(17, 26, '1973.jpg', 0),
(18, 26, '5927.jpg', 0),
(19, 27, '5469.jpg', 0),
(20, 27, '1446.jpg', 0),
(21, 27, '1670.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `producttype`
--

CREATE TABLE IF NOT EXISTS `producttype` (
  `id` int(11) NOT NULL,
  `typename` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_time` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `producttype`
--

INSERT INTO `producttype` (`id`, `typename`, `status`, `date`, `date_time`) VALUES
(1, 'Best Seller Fiction Books', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(2, 'Special Fiction Books', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(3, 'Popular Fiction Books', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(4, 'Newly Released Books', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(5, 'Humayun Ahmed Special Souvenir', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(6, 'Books of West Bengal', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(7, 'Editor&#039;s Pick', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(8, 'Last Books of Humayun Ahmed', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(9, 'Best Seller Outsourcing Book', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(10, 'Best Seller Programming Book', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(11, 'Cooking, Recipe ', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(12, 'Islami Books', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(13, 'New Books in Crime, Thrillers ', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(14, 'Books of HSC', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(15, 'Translated Books', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(16, 'Biographies ', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(17, 'History And Politics', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(18, 'Comics', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(19, 'Books of Engineering', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(20, 'Books Of MedicalSee', 'Yes', 'Dec 07, 2014', 'Dec 07, 2014'),
(21, 'Economic Books', 'Yes', 'Mar 02, 2015', 'March 2, 2015, 9:44 am'),
(22, 'shongrohoshala Book', 'Yes', 'Mar 09, 2015', 'March 9, 2015, 9:06 pm'),
(24, 'Test Product type', 'Yes', 'Sep 17, 2015', 'September 17, 2015, 7:28 pm'),
(25, 'Food', 'Yes', '2016-05-29', '2016-05-29 13:18:28'),
(26, 'Clothing', 'Yes', '2016-06-01', '2016-06-01 06:51:40');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
  `id` int(11) NOT NULL,
  `fullname` varchar(200) CHARACTER SET utf8 NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(200) CHARACTER SET utf8 NOT NULL,
  `alternate_phn` text NOT NULL,
  `gender` varchar(100) NOT NULL,
  `country` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `delivery_address` text CHARACTER SET utf8 NOT NULL,
  `password` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_time` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `fullname`, `email`, `mobile`, `alternate_phn`, `gender`, `country`, `city`, `area`, `delivery_address`, `password`, `date`, `date_time`) VALUES
(1, 'Mozahid Rana', 'mozahid32@gmail.com', '01684657278', '01829102857', '', 1, 1, 1, 'Tangar char', '123456', 'Dec 07, 2014', 'Dec 07, 2014, 11:10 pm'),
(2, 'Rasel', 'raselranawebsoft@gmail.com', '01829102857', '', '', 0, 0, 0, '', 'd515518a1715bae5fbfc59d2458de532', 'Dec 08, 2014', 'Dec 08, 2014, 12:11 am'),
(3, 'Miash Jaan Riya', 'maishajaan32@gmail.com', '01684979854', '01684657278', 'Female', 1, 2, 3, 'Uttara, Dhaka', '16a30c53858f533fb6f70d1eff523ad5', 'Jan 11, 2015', 'Jan 11, 2015, 2:29 am'),
(4, 'Ayesha Akter', 'cse_kusum@yahoo.com', '01736738267', '', 'Female', 0, 0, 0, 'mirpur', '47bce5c74f589f4867dbd57e9ca9f808', 'Jan 19, 2015', 'Jan 19, 2015, 12:15 pm'),
(5, 'Sheikh Abdullah', 'ranabetofen29@gmail.com', '01672428417', '', '', 0, 0, 0, '', '2eb5a42705deca5350087fe7f0621f57', 'Jan 19, 2015', 'Jan 19, 2015, 8:04 pm'),
(6, 'Jane Alam', 'jane@gmail.com', '01876767698', '', '', 0, 0, 0, '', '202cb962ac59075b964b07152d234b70', 'Jan 21, 2015', 'Jan 21, 2015, 12:15 am'),
(7, 'Jakir Hossain', 'jakir24434@yahoo.com', '01718304817', '01916189501', '', 1, 2, 8, 'House-12, road-4, block-2, sector-06 dhaka-1215', '5b119966ea959870a153d0e9ecd703d7', 'Jan 22, 2015', 'Jan 22, 2015, 12:13 am'),
(8, 'Rasel', 'mozahidodesk1@gmail.com', '01684657241', '', '', 0, 0, 0, '', 'be38e8e1b11ec03a7d25b515e1bbecde', 'Jan 22, 2015', 'Jan 22, 2015, 1:43 am'),
(9, 'Rasel', 'mozahidodesk@gmail.com', '012365481233', '', '', 0, 0, 0, '', 'cd61ab77ae48c24dfa518be5deaae91e', 'Jan 22, 2015', 'Jan 22, 2015, 1:51 am'),
(10, 'Din Mohammad', 'delu1786@gmail.com', '01552668359', '', '', 1, 2, 8, 'dfdfgdgdg', 'e10adc3949ba59abbe56e057f20f883e', 'Jan 27, 2015', 'Jan 27, 2015, 7:20 pm'),
(11, 'abcd', 'abcd@gmail.com', '012365478911', '', '', 1, 1, 1, 'abcd', 'e2fc714c4727ee9395f324cd2e7f331f', 'Sep 17, 2015', 'Sep 17, 2015, 11:45 am'),
(12, 'Tasnim Moon', 'moon.tasnim@yahoo.com', '0123654799333', '852147963210', 'Female', 1, 2, 8, 'abcd', '5a349aca4a3ec87faf4197f115f0104a', 'Sep 17, 2015', 'Sep 17, 2015, 12:41 pm'),
(13, 'Rifat', 'rifatcse09@gmail.com', '01670840934', 'dfg', '', 1, 2, 3, 'dfgfdgf', '5670b97ac1c6fd21222e8dfe77a4128f', 'Dec 09, 2015', 'Dec 09, 2015, 3:54 pm'),
(14, 'shohag', 'shohag@gmail.com', '01745114377', 'dffg', '', 1, 2, 8, 'dd', 'e10adc3949ba59abbe56e057f20f883e', '', ''),
(15, 'Md. saiful islam', 'siful.brur@yahoo.com', '01738154021', '01738154022', '', 1, 3, 7, 'badda', '202cb962ac59075b964b07152d234b70', 'May 18, 2016', 'May 18, 2016, 12:26 pm'),
(16, 'hjdfh', 'shohag12@gmail.com', '32487923711', '23151541333', '', 2, 6, 9, 'dfhxgj', 'e10adc3949ba59abbe56e057f20f883e', 'May 23, 2016', 'May 23, 2016, 4:12 pm');

-- --------------------------------------------------------

--
-- Table structure for table `root_category`
--

CREATE TABLE IF NOT EXISTS `root_category` (
  `id` int(11) NOT NULL,
  `root_cat_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `cat_status` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` varchar(100) NOT NULL,
  `date_time` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `root_category`
--

INSERT INTO `root_category` (`id`, `root_cat_name`, `cat_status`, `status`, `date`, `date_time`) VALUES
(26, 'Contact', 'No', 'Yes', '2016-05-24', '2016-05-24 13:12:41'),
(1, 'Book ', 'No', 'Yes', 'Dec 04, 2014', 'Dec 04, 2014'),
(3, 'Lady', 'No', 'Yes', 'Dec 04, 2014', 'Dec 04, 2014'),
(4, 'Book Lists', 'No', 'Yes', 'Dec 04, 2014', 'Dec 04, 2014'),
(5, 'Boi Mela 2016', 'No', 'Yes', 'Dec 04, 2014', 'Dec 04, 2014'),
(17, 'Dress', 'No', 'Yes', '2016-05-18', ''),
(27, 'Rice', 'Yes', 'No', '2016-05-29', '2016-05-29 13:16:02'),
(22, 'Shari', 'No', 'Yes', '2016-05-23', '2016-05-23 11:17:59'),
(25, 'Well Come', 'No', 'Yes', '2016-05-24', '2016-05-24 12:48:45'),
(24, 'Mens', 'Yes', 'Yes', '2016-05-23', '2016-05-23 11:45:17');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` int(11) NOT NULL,
  `rootcat_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `subcat_name` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(200) NOT NULL,
  `date` varchar(123) NOT NULL,
  `date_time` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `rootcat_id`, `cat_id`, `subcat_name`, `status`, `date`, `date_time`) VALUES
(1, 1, 1, 'কবিতা', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:09 am'),
(2, 1, 1, 'উপন্যাস', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:10 am'),
(3, 1, 1, 'রহস্য,গোয়েন্দা,ভৌতিক ও থৄলার', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:10 am'),
(4, 1, 1, 'সায়েন্স ফিকশন', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:11 am'),
(5, 1, 1, 'গল্প', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:11 am'),
(6, 1, 1, 'প্রবন্ধ', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:11 am'),
(7, 1, 1, 'পুরস্কারপ্রাপ্ত', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:11 am'),
(8, 1, 1, 'রচনাসমগ্র', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:12 am'),
(9, 1, 1, 'রূপকথা', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:12 am'),
(10, 1, 1, 'ইতিহাস ও ঐতিহ্য', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:12 am'),
(11, 1, 1, 'মুক্তিযুদ্ধ ও যুদ্ধবিগ্রহ', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:12 am'),
(12, 1, 1, 'ভ্রমণ ও প্রবাস', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:13 am'),
(13, 1, 1, 'জীবনী ও স্মৃতিচারণ', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:14 am'),
(14, 1, 1, 'গনিত,বিজ্ঞান ও প্রযুক্তি', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:14 am'),
(15, 1, 1, 'রাজনীতি', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:14 am'),
(16, 1, 1, 'সমাজ, সভ্যতা ও সংস্কৃতি', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:14 am'),
(17, 1, 1, 'বাংলাদেশ', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:15 am'),
(18, 1, 1, 'গণমাধ্যম ও সাংবাদিকতা', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:15 am'),
(19, 1, 1, 'নাটকের বই', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:15 am'),
(20, 1, 1, 'ক্লাসিক', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:16 am'),
(21, 1, 1, 'কৃষি ও কৃষক', 'Yes', 'Dec 14, 2014', 'December 14, 2014, 3:16 am'),
(22, 17, 92, 'Test sub category', 'Yes', 'Sep 17, 2015', 'September 17, 2015, 7:26 pm'),
(23, 17, 92, 'Gengy', 'No', '2016-05-25', '2016-05-25 07:08:31'),
(24, 22, 96, 'Silk jamdani', 'Yes', '2016-05-25', '2016-05-25 08:09:54'),
(25, 17, 92, 'Gengy T', 'Yes', '2016-05-29', '2016-05-29 13:17:00'),
(26, 17, 92, 'Pollo Tishert', 'Yes', '2016-06-01', '2016-06-01 06:49:55'),
(27, 22, 3, 'Silk jamdani', 'Yes', '2016-06-01', '2016-06-01 14:02:29');

-- --------------------------------------------------------

--
-- Table structure for table `superadmin`
--

CREATE TABLE IF NOT EXISTS `superadmin` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `superadmin`
--

INSERT INTO `superadmin` (`id`, `full_name`, `username`, `password`) VALUES
(1, 'admin', 'superadmin', '5670b97ac1c6fd21222e8dfe77a4128f'),
(2, 'shohag', 'admin', 'e10adc3949ba59abbe56e057f20f883e'),
(3, 'admin', 'shohag', 'fcea920f7412b5da7be0cf42b8c93759');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ad`
--

CREATE TABLE IF NOT EXISTS `tbl_ad` (
  `id` int(11) NOT NULL,
  `title` varchar(400) CHARACTER SET utf8 NOT NULL,
  `ad_img` varchar(200) NOT NULL,
  `ad_href` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_time` text NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ad`
--

INSERT INTO `tbl_ad` (`id`, `title`, `ad_img`, `ad_href`, `date`, `date_time`, `status`) VALUES
(1, 'How to buy book from online', '1425218157.jpg', '#', 'Yes', 'Jan 15, 2015', 'Yes'),
(3, 'Kishor Alo', '1425245393.jpg', '#', 'Jan 17, 2015', 'January 17, 2015, 5:06 am', 'Yes'),
(4, 'Cash on delivery', '1425409024.jpg', '#', 'Jan 17, 2015', 'January 17, 2015, 6:43 am', 'Yes'),
(5, 'Service', '1425245515.jpg', '#', 'Jan 17, 2015', 'January 17, 2015, 6:44 am', 'Yes'),
(6, 'shongrohoshala', '1425245704.jpg', '#', 'Mar 02, 2015', 'March 2, 2015, 1:53 am', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE IF NOT EXISTS `tbl_country` (
  `id` int(11) NOT NULL,
  `country_name` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_time` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`id`, `country_name`, `status`, `date`, `date_time`) VALUES
(1, 'বাংলাদেশ', 'No', 'Jan 04, 2015', 'January 4, 2015, 9:17 am'),
(2, 'India', 'Yes', 'May 18, 2016', 'May 18, 2016, 11:11 pm'),
(3, '', 'Yes', 'May 19, 2016', 'May 19, 2016, 8:17 pm'),
(4, 'dssd', 'Yes', 'May 19, 2016', 'May 19, 2016, 8:35 pm'),
(5, 'sds', 'Yes', 'May 19, 2016', 'May 19, 2016, 8:38 pm');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_district`
--

CREATE TABLE IF NOT EXISTS `tbl_district` (
  `id` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `district` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_time` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_district`
--

INSERT INTO `tbl_district` (`id`, `country`, `district`, `status`, `date`, `date_time`) VALUES
(1, 1, 'মুন্সীগঞ্জ', 'Yes', 'Jan 05, 2015', 'January 5, 2015, 4:14 am'),
(2, 1, 'ঢাকা', 'Yes', 'Jan 05, 2015', 'January 5, 2015, 4:14 am'),
(3, 1, 'নারায়নগঞ্জ', 'Yes', 'Jan 05, 2015', 'January 5, 2015, 4:14 am'),
(4, 1, 'মানিকগঞ্জ', 'Yes', 'Jan 05, 2015', 'January 5, 2015, 4:15 am'),
(5, 1, 'নরসিংদী', 'Yes', 'Jan 05, 2015', 'January 5, 2015, 4:15 am'),
(6, 2, 'Kholkhata', 'Yes', 'May 18, 2016', 'May 18, 2016, 11:11 pm');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_footerscroll`
--

CREATE TABLE IF NOT EXISTS `tbl_footerscroll` (
  `id` int(11) NOT NULL,
  `footer_title` longtext CHARACTER SET utf8 NOT NULL,
  `status` varchar(20) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_time` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_footerscroll`
--

INSERT INTO `tbl_footerscroll` (`id`, `footer_title`, `status`, `date`, `date_time`) VALUES
(7, '*** সংগ্রহশালা ডট কম -এ আসুন, অর্ডার করুন। আমরাই সবথেকে দ্রুত পৌঁছে দেবো আপনার প্রিয় বইটি আপনারই হাতে ***', 'Yes', 'Feb 11, 2015', 'February 11, 2015, 9:56 pm'),
(8, '*** আগে পণ্য বুঝে নিন, \r\nপরে টাকা দিন ***\r\nকিনতে কিনতে পড়ুন ***\r\nবই আপনার সবথেকে প্রিয় বন্ধু ***\r\nবই দিয়ে জীবন গড়ুন ***\r\nবইকে সাথী করুন, \r\nজ্ঞানকে উন্মুক্ত করুন ***\r\nবই কি একটি উপহার হতে পারে না? ***\r\nবইকে আপন করুন, \r\nজীবনকে ভালবাসুন ***\r\nবইয়ের নেশা আপনাকে করবে আরো আলোকিত ***\r\n\r\n', 'Yes', 'Mar 02, 2015', 'March 2, 2015, 12:53 am'),
(9, '*** প্রিয় মানুষটিকে বই ও টি-শার্ট উপহার  দিন *** প্রিয় মানুষটিকে বই বা টি- শার্ট উপহার দিতে  সংগ্রহশালা ডট কম \r\n-এ অর্ডার করুন *** এক্ষেত্রে আপনাকে b-kash এর মাধ্যমে \r\n01952898288 বা 01959525634 -এ অগ্রিম পেমেন্ট করতে হবে। তাহলেই অর্ডারকৃত বই ও টি-শার্ট টি পৌঁছে যাবে আপনার প্রিয় মানুষটির হাতে *** \r\n\r\n', 'Yes', 'Mar 02, 2015', 'March 2, 2015, 1:14 am'),
(10, 'কোনো কিছু জানতে/জানাতে মেইল করুন এখানে, \r\nসংগ্রহশালা ডট কম কাস্টমার কেয়ার/এডমিনঃ\r\nadmin@shongrohoshala.com ,\r\ninfo@shongrohoshala.com \r\n*** hotline:  +8801871011361, +8801871011362 ***', 'Yes', 'Mar 02, 2015', 'March 2, 2015, 1:15 am');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_details`
--

CREATE TABLE IF NOT EXISTS `tbl_order_details` (
  `order_id` int(12) NOT NULL,
  `uniq_id` int(12) NOT NULL,
  `customer_id` varchar(12) NOT NULL,
  `book_id` int(11) NOT NULL,
  `book_price` float(15,2) NOT NULL,
  `book_qty` int(11) NOT NULL,
  `order_status` varchar(200) NOT NULL,
  `status_date` varchar(100) NOT NULL,
  `order_date_time` varchar(111) NOT NULL,
  `order_from` varchar(111) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_details`
--

INSERT INTO `tbl_order_details` (`order_id`, `uniq_id`, `customer_id`, `book_id`, `book_price`, `book_qty`, `order_status`, `status_date`, `order_date_time`, `order_from`) VALUES
(1, 1420814628, '1', 4, 280.00, 41, 'Accepted', '0000-00-00', 'January 9, 2015, 11:46 pm', 'Cash On Delivery'),
(2, 1420825687, '1', 4, 280.00, 3, 'Accepted', '0000-00-00', 'January 9, 2015, 11:49 pm', 'Cash On Delivery'),
(3, 1420825687, '1', 1, 1240.00, 4, 'Accepted', '0000-00-00', 'January 9, 2015, 11:49 pm', 'Cash On Delivery'),
(4, 1420826071, '1', 4, 280.00, 5, 'Accepted', '0000-00-00', 'January 9, 2015, 11:57 pm', 'Cash On Delivery'),
(5, 1420826764, '1', 3, 1240.00, 6, 'Accepted', 'January 10, 2015', 'January 10, 2015, 12:27 am', 'Cash On Delivery'),
(6, 1420922111, '3', 5, 575.00, 5, 'Accepted', 'January 11, 2015', 'January 11, 2015, 2:35 am', 'Cash On Delivery'),
(7, 1421867896, '7', 7, 1240.00, 0, 'Accepted', 'January 22, 2015', 'January 22, 2015, 12:29 am', 'Cash On Delivery'),
(8, 1422332365, '3', 6, 750.00, 5, 'Processing', 'January 27, 2015', 'January 27, 2015, 9:21 am', 'Cash On Delivery'),
(9, 1422368472, '10', 1, 1240.00, 1, 'Processing', 'January 27, 2015', 'January 27, 2015, 7:22 pm', 'Cash On Delivery'),
(10, 1422368472, '10', 8, 1240.00, 3, 'Processing', 'January 27, 2015', 'January 27, 2015, 7:22 pm', 'Cash On Delivery'),
(11, 1442465210, '11', 1, 1240.00, 3, 'Processing', 'September 17, 2015', 'September 17, 2015, 11:47 am', 'Cash On Delivery'),
(12, 1442465210, '11', 6, 750.00, 4, 'Accepted', 'September 17, 2015', 'September 17, 2015, 11:47 am', 'Cash On Delivery'),
(13, 1442468414, '12', 17, 4000.00, 4, 'Processing', 'September 17, 2015', 'September 17, 2015, 12:45 pm', 'Cash On Delivery'),
(14, 1442468414, '12', 16, 350.00, 3, 'Accepted', 'September 17, 2015', 'September 17, 2015, 12:45 pm', 'Cash On Delivery'),
(15, 1449657852, '13', 1, 1240.00, 4, 'Processing', 'December 9, 2015', 'December 9, 2015, 4:45 pm', 'Cash On Delivery'),
(16, 1450698003, '13', 10, 1240.00, 2, 'Processing', 'December 21, 2015', 'December 21, 2015, 6:08 pm', 'Cash On Delivery'),
(17, 1450698003, '13', 11, 1240.00, 5, 'Processing', 'December 21, 2015', 'December 21, 2015, 6:08 pm', 'Cash On Delivery'),
(18, 1463378029, '14', 1, 1240.00, 2, 'Accepted', 'May 16, 2016', 'May 16, 2016, 5:02 pm', 'Cash On Delivery'),
(19, 1463378029, '14', 7, 1240.00, 3, 'Accepted', 'May 16, 2016', 'May 16, 2016, 5:02 pm', 'Cash On Delivery'),
(20, 1463378029, '14', 6, 750.00, 1, 'Accepted', 'May 16, 2016', 'May 16, 2016, 5:02 pm', 'Cash On Delivery'),
(21, 1463378029, '14', 4, 280.00, 2, 'Accepted', 'May 16, 2016', 'May 16, 2016, 5:02 pm', 'Cash On Delivery'),
(22, 1463548831, '15', 1, 1240.00, 1, 'Accepted', 'May 18, 2016', 'May 18, 2016, 12:30 pm', 'Cash On Delivery'),
(23, 1463994660, '16', 1, 1240.00, 2, 'Processing', 'May 23, 2016', 'May 23, 2016, 4:13 pm', 'Cash On Delivery'),
(24, 1463994812, '16', 1, 1240.00, 2, 'Accepted', 'May 23, 2016', 'May 23, 2016, 4:51 pm', 'Cash On Delivery'),
(25, 1464842519, '16', 6, 750.00, 3, 'Processing', 'June 5, 2016', 'June 5, 2016, 4:18 pm', 'Cash On Delivery'),
(26, 1464842519, '16', 1, 1240.00, 2, 'Processing', 'June 5, 2016', 'June 5, 2016, 4:18 pm', 'Cash On Delivery'),
(27, 1464842519, '16', 7, 1240.00, 7, 'Processing', 'June 5, 2016', 'June 5, 2016, 4:18 pm', 'Cash On Delivery'),
(28, 1464842519, '16', 8, 1240.00, 8, 'Processing', 'June 5, 2016', 'June 5, 2016, 4:18 pm', 'Cash On Delivery');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pagecontent`
--

CREATE TABLE IF NOT EXISTS `tbl_pagecontent` (
  `id` int(11) NOT NULL,
  `page_typ` varchar(40) NOT NULL,
  `content` longtext CHARACTER SET utf8 NOT NULL,
  `status` varchar(110) NOT NULL,
  `date` varchar(100) NOT NULL,
  `date_time` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pagecontent`
--

INSERT INTO `tbl_pagecontent` (`id`, `page_typ`, `content`, `status`, `date`, `date_time`) VALUES
(3, 'Contact', '<p><span style="font-size: medium;"><br />সংগ্রহশালা ডট কম</span><br /><span style="font-size: medium;">হাবিব সুপার মার্কেট</span><br /><span style="font-size: medium;">প্লট-১৩, রোড-০৩, সেনপাড়া পর্বতা &nbsp; &nbsp; &nbsp;</span><br /><span style="font-size: medium;">মিরপুর-১০,&nbsp;ঢাকা &ndash; ১২১৬ </span><br /><span style="font-size: medium;">বাংলাদেশ ।&nbsp;</span><br />&nbsp;<br /><span style="font-size: medium;">ই-মেইলঃ &nbsp;<a href="mailto:info@shongrohoshala.com">info@shongrohoshala.com</a></span><br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="mailto:admin@shogrohoshala.com"><span style="font-size: medium;">admin@shongrohoshala.com</span><br /></a></p>', 'Yes', 'Mar 03, 2015', 'March 3, 2015, 6:35 am'),
(8, 'About', '<p><span style="font-size: x-large;"><span style="color: #0000ff;"><strong><span style="text-decoration: underline;"><br /><br />সংগ্রহশালা <span style="color: #ff0000;">ডট</span></span></strong><span style="color: #ff0000;"><strong><span style="text-decoration: underline;"> </span></strong></span><strong><span style="text-decoration: underline;">কম -এর পদযাত্রাঃ</span></strong><strong><span style="text-decoration: underline;"> </span></strong></span><strong>&nbsp;</strong></span></p>\r\n<p><span style="font-size: medium;"><span style="font-size: large;"><br /><span style="color: #008000;"><strong>সংগ্রহশালা<span style="font-size: xx-large; color: #ff0000;">.</span>কম</strong></span></span><strong> (</strong><a href="http://www.shongrohoshala.com/">www.shongrohoshala.com</a>) ৭ই মার্চ ২০১৫ ইং তার যাত্রা আরম্ভ করে। <strong><span style="font-size: large; color: #008000;">সংগ্রহশালা<span style="font-size: xx-large; color: #ff0000;">.</span>কম&nbsp;</span> IIHDB Ltd. </strong>(<strong>আইআইএইচডিবি লিঃ</strong>) -এর একটি অঙ্গ প্রতিষ্ঠান। &nbsp;</span></p>\r\n<p><span style="font-size: medium;"><span style="color: #008000;"><strong><span style="font-size: large;"><br />সংগ্রহশালা<span style="font-size: xx-large; color: #ff0000;">.</span>কম</span></strong></span> (<a href="http://www.shongrohoshala.com/">www.shongrohoshala.com</a>) -এ আপনি আপনার প্রিয় লেখক, প্রিয় প্রকাশনীর যে কোনো বই পাবেন খুব সহজে।</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="font-size: medium;">আমরা জানাচ্ছি যে, <span style="color: #008000;"><strong><span style="font-size: large;">সংগ্রহশালা<span style="color: #ff0000; font-size: xx-large;">.</span>কম</span></strong></span> (<a href="http://www.shongrohoshala.com/">www.shongrohoshala.com</a>) একটি অনলাইনে বাংলা বইয়ের বিশাল সংগ্রহশালা। <br /><br />এছাড়া <span style="color: #008000;"><strong><span style="font-size: large;">সংগ্রহশালা<span style="font-size: xx-large; color: #ff0000;">.</span>কম</span></strong></span> (<a href="http://www.shongrohoshala.com/">www.shongrohoshala.com</a>) আপনাদেরকে দিচ্ছে সুন্দর সুন্দর টি-শার্ট খুবই কম দামে। বই এবং টি-শার্ট &nbsp;কিনতে <span style="color: #008000;"><strong><span style="font-size: large;">সংগ্রহশালা<span style="font-size: xx-large; color: #ff0000;">.</span>কম</span></strong></span> (<a href="http://www.shongrohoshala.com/">www.shongrohoshala.com</a>) -এ আসুন। পছন্দ মতো বই বেছে অর্ডার করুন আমাদেরকে। আমরাই আপনার হাতে পৌঁছে দিচ্ছি সবথেকে কম খরচে, সবথেকে কম সময়ে আপনারই প্রিয় বইটি আপনারই হাতে। বই আগে বুঝে নিয়ে পরে আমাদেরকে টাকা দিন। আপনার পছন্দের মানুষটিকে বই কিংবা টি-শার্ট উপহার দিতে আপনি চাইলে আমাদের সাহায্য নিতে পারেন। আমরা আপনার প্রিয় মানুষটির হাতে পৌঁছে দেবো আপনারই অর্ডার দেয়া বই কিংবা টি-শার্টটি খুব দ্রুত। <br /><br /><span style="color: #008000;"><strong><span style="font-size: large;">সংগ্রহশালা<span style="font-size: xx-large; color: #ff0000;">.</span>কম</span></strong></span> (<a href="http://www.shongrohoshala.com/">www.shongrohoshala.com</a>) বাংলাদেশে আরো আরো পাঠক তৈরিতে কাজ করছে নিরন্তর। আমরা আপনারই পাশে আছি এবং থাকতে চাই চিরদিন।</span></p>\r\n<p><span style="font-size: medium;"><span style="color: #008000;"><strong><span style="font-size: large;"><br />সংগ্রহশালা<span style="color: #ff0000; font-size: xx-large;">.</span>কম</span></strong></span> (<a href="http://www.shongrohoshala.com/">www.shongrohoshala.com</a>) আপনাদের সেবায় সর্বদা নিয়োজিত থাকবে এই প্রত্যাশা আপনারা করতেই পারেন।</span></p>\r\n<p><span style="font-size: medium;"><br />আপনার প্রিয় বইটি পেতে আমাদের <span style="color: #008000;"><strong><span style="font-size: large;">সংগ্রহশালা<span style="color: #ff0000; font-size: xx-large;">.</span>কম</span></strong> </span>(<a href="http://www.shongrohoshala.com/">www.shongrohoshala.com</a>) এই সাইটে ভিজিট করুন এবং অর্ডার দিন অথবা আমাদেরকে কল করুন অথবা মেইল করুন।</span></p>\r\n<p><span style="font-size: medium;"><br />যেকোনো পরামর্শ অথবা আপনার পছন্দ-অপছন্দ জানাতে আমাদেরকে জানান মন খুলে। আমরা আপনার পরামর্শ এবং চাহিদাকে গুরুত্ব দেবো সর্বোচ্চ । &nbsp;&nbsp;&nbsp;&nbsp;</span></p>\r\n<p><span style="font-size: medium;"><span style="color: #008000;"><strong><span style="font-size: large;"><br />সংগ্রহশালা<span style="font-size: xx-large; color: #ff0000;">.</span>কম</span></strong></span> (<a href="http://www.shongrohoshala.com/">www.shongrohoshala.com</a>) বাংলা বইয়ের প্রচার ও প্রসারে কাজ করছে।&nbsp;</span></p>', 'Yes', 'Mar 09, 2015', 'March 9, 2015, 9:32 pm');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thana`
--

CREATE TABLE IF NOT EXISTS `tbl_thana` (
  `id` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `district` int(11) NOT NULL,
  `thana` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_time` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_thana`
--

INSERT INTO `tbl_thana` (`id`, `country`, `district`, `thana`, `status`, `date`, `date_time`) VALUES
(1, 1, 1, 'গজারিয়া', 'Yes', 'Jan 05, 2015', 'January 5, 2015, 8:39 am'),
(3, 1, 2, 'মাইশা', 'Yes', 'Jan 05, 2015', 'January 5, 2015, 8:39 am'),
(7, 1, 3, 'Maisha', 'Yes', 'Jan 06, 2015', 'January 6, 2015, 1:46 am'),
(8, 1, 2, 'Uttara', 'Yes', 'Jan 08, 2015', 'January 8, 2015, 4:57 am'),
(9, 2, 6, 'durmar', 'Yes', 'May 18, 2016', 'May 18, 2016, 11:12 pm');

-- --------------------------------------------------------

--
-- Table structure for table `temp_details`
--

CREATE TABLE IF NOT EXISTS `temp_details` (
  `temp_id` int(12) NOT NULL,
  `user_id` varchar(222) NOT NULL,
  `book_id` int(12) NOT NULL,
  `book_qty` float(11,2) NOT NULL,
  `book_price` float(15,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_details`
--

INSERT INTO `temp_details` (`temp_id`, `user_id`, `book_id`, `book_qty`, `book_price`) VALUES
(1465358329, '', 22, 3.00, 300.00),
(1465358329, '', 22, 5.00, 300.00),
(1465358329, '', 6, 3.00, 750.00),
(1465358329, '', 22, 5.00, 300.00),
(1465358329, '', 22, 4.00, 300.00),
(1465358329, '', 22, 4.00, 300.00),
(1465368692, '', 22, 2.00, 300.00),
(1465358329, '', 25, 4.00, 320.00),
(1465368692, '', 22, 4.00, 300.00),
(1465358329, '', 25, 1.00, 320.00),
(1465358329, '', 25, 6.00, 320.00),
(1465368692, '', 22, 1.00, 300.00),
(1465368692, '', 26, 3.00, 12000.00);

-- --------------------------------------------------------

--
-- Table structure for table `temp_order`
--

CREATE TABLE IF NOT EXISTS `temp_order` (
  `temp_id` int(12) NOT NULL,
  `user_id` varchar(222) NOT NULL,
  `uniq_id` int(33) NOT NULL,
  `temp_date_time` varchar(111) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_order`
--

INSERT INTO `temp_order` (`temp_id`, `user_id`, `uniq_id`, `temp_date_time`) VALUES
(1, '', 1465368692, 'June 8, 2016, 8:51 am');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`code`),
  ADD KEY `continent_code` (`continent_code`);

--
-- Indexes for table `image_product`
--
ALTER TABLE `image_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `producttype`
--
ALTER TABLE `producttype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `root_category`
--
ALTER TABLE `root_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `superadmin`
--
ALTER TABLE `superadmin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ad`
--
ALTER TABLE `tbl_ad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_country`
--
ALTER TABLE `tbl_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_district`
--
ALTER TABLE `tbl_district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_footerscroll`
--
ALTER TABLE `tbl_footerscroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_pagecontent`
--
ALTER TABLE `tbl_pagecontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_thana`
--
ALTER TABLE `tbl_thana`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_order`
--
ALTER TABLE `temp_order`
  ADD PRIMARY KEY (`temp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `image_product`
--
ALTER TABLE `image_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `producttype`
--
ALTER TABLE `producttype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `root_category`
--
ALTER TABLE `root_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `superadmin`
--
ALTER TABLE `superadmin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_ad`
--
ALTER TABLE `tbl_ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_country`
--
ALTER TABLE `tbl_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_district`
--
ALTER TABLE `tbl_district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_footerscroll`
--
ALTER TABLE `tbl_footerscroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  MODIFY `order_id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tbl_pagecontent`
--
ALTER TABLE `tbl_pagecontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_thana`
--
ALTER TABLE `tbl_thana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `temp_order`
--
ALTER TABLE `temp_order`
  MODIFY `temp_id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
