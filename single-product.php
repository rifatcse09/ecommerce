<?php
	include('include/header.php');
	
	$bookid = htmlspecialchars($_REQUEST['book_id'], ENT_QUOTES, 'UTF-8');
	

	
	$pro = $cls_root_category->product_view_id($bookid);
	
	$data_row = $pro->fetch_assoc();
	
	$product_id = $data_row['id'];
	$price = $data_row['price'];
	$btitile = $data_row['title'];
	
	
?>
    <div class="maincontent-area">
        <div class="zigzag-bottom">
		
		</div>
       
    </div> <!-- End main content area -->
<div class="container">
	<div class="row">
	<div class="col-md-12">
			<div class="span_1_of_left">
				<div class="grid images_3_of_2">
					<div class="flexslider">
							        <!-- FlexSlider -->
										<script src="js/imagezoom.js"></script>
										  <script defer src="js/jquery.flexslider.js"></script>
										<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />

										<script>
										// Can also be used with $(document).ready()
										$(window).load(function() {
										  $('.flexslider').flexslider({
											animation: "slide",
											controlNav: "thumbnails"
										  });
										});
										</script>
									<!-- //FlexSlider-->
							  <ul class="slides">
								<?php 
								
								$img = $data_row['id'];
								
								$data_img = $cls_root_category->book_page_view_image_all($img);
								
								while($imag = $data_img->fetch_assoc()){ 
									
									
								
								?>
								
								<li data-thumb="apanel/uploaded/<?php echo $imag['photo']; ?>">
									<div class="thumb-image"> <img src="apanel/uploaded/<?php echo $imag['photo']; ?>" data-imagezoom="apanel/uploaded/<?php echo $imag['photo']; ?>" class="img-responsive"> </div>
								</li>
								
								<?php } ?>
							  </ul>
							<div class="clearfix"></div>
					</div>		
				</div>
			</div>
			<!-- start span1_of_1 -->
			<div class="span1_of_1_des">
				  <div class="desc1">
					<h2 style="text-transform: uppercase;"><?php echo $data_row['title']; ?></h2>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
					
					<div class="available">
						<h4>Available Options :</h4>
						<ul>
							<li>Color:
								<select id="colorsss" name="color">
								<option value="Silver">Silver</option>
								<option value="Black">Black</option>
								<option value="Dark Black">Dark Black</option>
								<option value="Red">Red</option>
							</select></li>
							<li>Size:
								<select id="size" name="size">
									<option value="L">L</option>
									<option value="XL">XL</option>
									<option value="S">S</option>
									<option value="M">M</option>
								</select>
							</li>
							<li>Quality:
								<input type="text" style="width:60px;height:30px;border:1px solid #555555;padding-left:5px;" name="quantity" id="quantity">
							</li>
						</ul>
						<div class="btn_form">
						
							<a href="#" class="blue-btn" title="Buy Now" onClick="add_book_cart(<?php echo $product_id; ?>, <?php echo $price; ?>,'<?php echo $btitile; ?>');">Buy Now</a>
						</div>
						<span class="span_right"><h2 style="text-align:center;"><b class="price">৳ <?php echo $data_row['price']; ?></b></h2></span>
						<div class="clearfix"></div>
					</div>
				  <h2>Products Detials</h2>
				  <ul class="nav nav-tabs" style="background-color:#005C86;">
					<li class="active"><a data-toggle="tab" href="#home">Product Feature</a></li>
					<li><a data-toggle="tab" href="#menu1">Terms Condition</a></li>
					<li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
				  </ul>

				  <div class="tab-content">
					<div id="home" class="tab-pane fade in active" style="padding:17px;">
					<br>
					<?php echo $data_row['book_summary']; ?>
					</div>
					<div id="menu1" class="tab-pane fade" style="padding:17px;">
					  <br>
					  <?php echo $data_row['terms_condition']; ?>
					</div>
					<div id="menu2" class="tab-pane fade" style="padding:17px;">
					  <h3>Menu 2</h3>
					  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
					</div>
				  </div>
			</div>
		</div>
	<div class="clearfix"></div>
</div>
</div>
</div>		
      <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row-row">
                <div class="col-md-12">
                    <div class="latest-product">
                        <a href="#"><h2 class="section-title">Related Products</h2></a>
                        <div class="product-carousel">
						 <div class="single-product">
                                <div class="product-f-image">
                                    <img src="img/product-6.jpg" alt="">
                                    <div class="product-hover">
                                        <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <a href="#" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                    </div>
                                </div>
                                
                                <h2><a href="#">Samsung gallaxy note 4</a></h2>

                                <div class="product-carousel-price">
                                    <ins>$400.00</ins>
                                </div>                            
                            </div>
                            <div class="single-product">
                                <div class="product-f-image">
                                    <img src="img/product-1.jpg" alt="">
                                    <div class="product-hover">
                                        <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <a href="#" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                    </div>
                                </div>
                                
                                <h2><a href="#">Samsung Galaxy s5- 2015</a></h2>
                                
                                <div class="product-carousel-price">
                                    <ins>$700.00</ins> <del>$100.00</del>
                                </div> 
                            </div>
                            <div class="single-product">
                                <div class="product-f-image">
                                    <img src="img/product-2.jpg" alt="">
                                    <div class="product-hover">
                                        <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <a href="#" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                    </div>
                                </div>
                                
                                <h2>Nokia Lumia 1320</h2>
                                <div class="product-carousel-price">
                                    <ins>$899.00</ins> <del>$999.00</del>
                                </div> 
                            </div>
                            <div class="single-product">
                                <div class="product-f-image">
                                    <img src="img/product-3.jpg" alt="">
                                    <div class="product-hover">
                                        <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <a href="#" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                    </div>
                                </div>
                                
                                <h2>LG Leon 2015</h2>

                                <div class="product-carousel-price">
                                    <ins>$400.00</ins> <del>$425.00</del>
                                </div>                                 
                            </div>
                            <div class="single-product">
                                <div class="product-f-image">
                                    <img src="img/product-4.jpg" alt="">
                                    <div class="product-hover">
                                        <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <a href="#" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                    </div>
                                </div>
                                
                                <h2><a href="#">Sony microsoft</a></h2>

                                <div class="product-carousel-price">
                                    <ins>$200.00</ins> <del>$225.00</del>
                                </div>                            
                            </div>
                            <div class="single-product">
                                <div class="product-f-image">
                                    <img src="img/product-5.jpg" alt="">
                                    <div class="product-hover">
                                        <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <a href="#" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                    </div>
                                </div>
                                
                                <h2>iPhone 6</h2>

                                <div class="product-carousel-price">
                                    <ins>$1200.00</ins> <del>$1355.00</del>
                                </div>                                 
                            </div>
                            <div class="single-product">
                                <div class="product-f-image">
                                    <img src="img/product-6.jpg" alt="">
                                    <div class="product-hover">
                                        <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <a href="#" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                    </div>
                                </div>
                                
                                <h2><a href="#">Samsung gallaxy note 4</a></h2>

                                <div class="product-carousel-price">
                                    <ins>$400.00</ins>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End main content area -->
	
<?php
	include('include/footer.php');
?>