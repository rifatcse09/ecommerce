<?php
	include('include/header.php'); 
	
	//session_start();
	//session_destroy();
	//$uniq_id = $_SESSION['uniq_id'];
	
	$mb_q = $cls_additem->select_temp($uniq_id);
	
	$mb_r = $mb_q->fetch_assoc();
	$mbookId = $mb_r['book_id'];
	
	
	
?>
  
  
    <div class="single-cart-area">
        <div class="zigzag-bottom"></div>
		<div class="container">
			<div class="row-row">
			<div class="col-md-10 col-md-offset-1">
			<?php
				if(!empty($mbookId))
				{
			?>
			<h3><b>Your Shopping Cart</b></h3>
				<table>
					<tr>
						<th>SL</th>
						<th>ITEM</th>
						<th>QTY</th>
						<th>Unit Price</th>
						<th>Total</th>
						<th>Action</th>
					</tr>
					<?php 
					$sl = 1; $subtotal = 0; $ttlsub = 0; $total = 0; $shipping_chr = 50;
					
					$scart_q = $cls_additem->select_quntity($uniq_id);
					
					while($scart_r = $scart_q->fetch_array())
						
						{
							
						$mbook_id = $scart_r[0];
						$mbook_qty = $scart_r[1];
						$mbook_price = $scart_r[2];
						
						//sub total//
						$subtotal = $mbook_price*$mbook_qty;
						$sub_total_decimal = number_format($subtotal, 2, '.', ',');
						
						//total sub total//
						$ttlsub = $ttlsub + $subtotal;
						$ttlsub_dc = number_format($ttlsub, 2, '.', ',');
						
						//total tk//
						$total = $ttlsub + $shipping_chr;
						$total_dc = number_format($total, 2, '.', ',');
						
						
						$mbookinfo_q = $cls_additem->select_book_id($mbook_id);
						
						$mbookinfo_r = $mbookinfo_q->fetch_assoc();
						
						$mbimage = $mbookinfo_r['image'];
						$mbtitle = $mbookinfo_r['title'];
						$mbeditor = $mbookinfo_r['terms_condition'];
									
						//editor name//
						$edn_q = $cls_additem->select_category_name($mbeditor);
						$redm = $edn_q->fetch_array();
						$edname = $redm[0];
						
					?>
					
					<tr class="addcart">
						<td style="width:10px;">
							<p><?php echo $sl++; ?></p>
						</td>
						<td>
							<div class="cart-left">
							
								<a href="#"> <img src="apanel/title_image/<?php echo  $mbimage; ?>" width="60px" height="70px" alt=""/> </a>
								
							</div>
							<div class="cart-right">
								<p class="cart-p"> Basic Sneakers</p>
								<h5  class="cart-h5"><?php echo $mbtitle .' - <br>'.$edname; ?></h5>
								<p class="cart-p"> Seller: BR Bazar</p>
								
							</div>
						</td>
						<td style="width:100px;">
							<!--<input type="text" width="50px" class="form-control" name="quntity" value="2.00">-->
							<?php echo $mbook_qty; ?>
						</td>
						<td class="unit-price">
							<p>Tk:<?php echo $mbook_price; ?></p> 
							<p class="old-price">TK:520</p>
							<span class="save">Savings: TK 20</span> 
						</td>
						<td class="unit-total">
							TK:<?php echo $sub_total_decimal; ?>
						</td>
						<td style="width:80px;">
							<p class="cart-r"><a href="#" onClick="remove_cartitem('<?php echo $mbook_id; ?>');">Remove</a></p>
						</td>
					</tr>
					
					<?php } ?>
					
					<tr class="coupons">
						<th colspan="2">
							<span class="coupon">
							<p class="couponP">Coupon</p>
							<p class="couponT">Type your voucher code here</p>
							<form class="form-inline">
							<input type="text" class="form-control" placeholder="use coupon code" class="input-medium search-query">
							<button type="submit" class="btn">USE</button>
							</form>
						</span>
						</th>
						<th colspan="3">
							<span class="price-total">
							<P class="price-subtotal"> SUBTOTAL: TK.<?php echo $ttlsub_dc; ?></P>
							<P class="price-subtotal">Shipping: <?php echo $shipping_chr; ?></P>
							<P class="pricetotal"> TOTAL: TK.<?php if(!empty($total_dc)) { ?><?php echo $total_dc; }?></P>
							</span>
						</th>
					</tr>
					<tr>
						<th colspan="3">
							<p class="fianlprice"><span class="fianlsaving">Your total savings: <span class="pricecolor">TK.93.00</span></span></p>
						</th>
						<th class="shop_continue" colspan="2">
							<div class="shop_cont">
							<input type="button" onClick="javascript:location.replace('index');" name="bt" value="Continue Shopping" class="continue_shopping_btn">
							</div>
						</th>
						<th class="cart-chekout" colspan="1">
							<div class="chekout">
							<input type="button" onClick="javascript:location.replace('login');" name="bt" value="Place Order" class="placeorder_btn">
							</div>
						</th>
					</tr>
				</table>
				
				<?php
				} else {
				?>
				<div align="center" style="width:660px; margin:auto; font-size:24px; height:100px; border:1px solid #ccc; border-radius:4px;">Your Shopping cart Empty!!
				<?php echo $mbookId;?>
				
				</div>
				<?php
				}
				?>
					
			</div>
			</div>
		</div>
    </div>  
</div>
<?php
include('include/footer.php'); 

?>
  